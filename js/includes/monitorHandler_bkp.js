var monitorHandler = {};
monitorHandler.postponedSave = false;
monitorHandler.init = function () {
	$(".hubAdd").unbind("click");
	$(".hubAdd").bind("click", monitorHandler.hubAdd);
	
	$(".pageAdd").unbind("click");
	$(".pageAdd").bind("click", monitorHandler.pageAdd);
	
	$(".sessionAdd").unbind("click");
	$(".sessionAdd").bind("click", monitorHandler.sessionAdd);
	
	$(".postsAdd").unbind("click");
	$(".postsAdd").bind("click", monitorHandler.postsAdd);
	
	$(".notifyAdmins").unbind("click");
	$(".notifyAdmins").bind("click", monitorHandler.notifyAdmins);
	
	$("a[target=_blank]").unbind("click");
	$("a[target=_blank]").bind("click", function (event) {
		event = $.event.fix(event);
		if (event.preventDefault) event.preventDefault();
		
		return false;
	});
};
monitorHandler.initPosts = function () {
	$(".healthPlus, .healthMinus").unbind("click");
	$(".healthPlus, .healthMinus").bind("click", monitorHandler.healthClick);
	
	$(".topicAdd").unbind("click");
	$(".topicAdd").bind("click", monitorHandler.topicAdd);
	
	$(".heartIcon").unbind("click");
	$(".heartIcon").bind("click", monitorHandler.proximityClick);
	
	$(".intensityOK, .intensitySevere, .intensityCritical").unbind("click");
	$(".intensityOK, .intensitySevere, .intensityCritical").bind("click", monitorHandler.intensityClick);
	
	$("a[target=_blank]").unbind("click");
	$("a[target=_blank]").bind("click", function (event) {
		event = $.event.fix(event);
		if (event.preventDefault) event.preventDefault();
		
		return false;
	});
	
	$(".loadMore").bind("click", function (event) {
		event = $.event.fix(event);
		if (event.preventDefault) event.preventDefault();
		
		$(this).text(TXT_MONITORING_LOADING_MORE);
		
		monitorHandler.showHide(this);
	});
	
	$("select[name^=topicID]").bind("focus", function () {
		$(this).data({prevVal: $(this).val()});
	});
	$("select[name^=topicID]").bind("change", function () {
		var newVal = $(this).val();
		
		if ($(this).data("prevVal") == -1)
		{
			$("select", $(this).parents(".monitoredPage")).each(function () {
				if ($(this).val() == -1)
				{
					$(this).val(newVal);
				}
			});
		}
	});
};
monitorHandler.healthClick = function (event) {
	event = $.event.fix(event);
	if (event.preventDefault) event.preventDefault();
	
	var anchor = $(event.target);
	while ($(anchor).get(0).nodeName.toLowerCase() != "a")
	{
		anchor = $(anchor).parents("a");
	}
	
	var tr = $(event.target);
	while ($(tr).get(0).nodeName.toLowerCase() != "tr")
	{
		tr = $(tr).parents("tr");
	}
	
	if ($(anchor).hasClass("healthPlus"))
	{
		if (parseInt($("[name^=postHealth]", $(tr)).val()+1) <= 1)
		{
			$("[name^=postHealth]", $(tr)).val(parseInt($("[name^=postHealth]", $(tr)).val())+1);
		}
	}
	else
	{
		if ($("[name^=postHealth]", $(tr)).val()-1 >= -1)
		{
			$("[name^=postHealth]", $(tr)).val(parseInt($("[name^=postHealth]", $(tr)).val())-1);
		}
	}
	
	var flagTitle = TXT_POST_HEALTH+": ";
	var src = BASE_URL+"images/icons/editing_controls/24/";
	switch (parseInt($("[name^=postHealth]", $(tr)).val()))
	{
		case -1:
			src += "Emoticon_Angry.png";
			flagTitle += TXT_POST_FLAG_RED;
			break;
		case 0:
			src += "Emoticon_Tense.png";
			flagTitle += TXT_POST_FLAG_BLUE;
			break;
		case 1:
			src += "Emoticon_Smile.png";
			flagTitle += TXT_POST_FLAG_GREEN;
			break;
	}
	
	$(".postHealthIMG", $(tr)).attr({
		src: src,
		title: flagTitle
	});
	
	if (parseInt($("[name^=postHealth]", $(tr)).val()) <= 0)
	{
		$(".healthPlus img", $(tr)).attr("src", BASE_URL+"images/icons/commerce/24/Rate_Thumbs_Up.png");
		if (parseInt($("[name^=postHealth]", $(tr)).val()) < 0)
		{
			$(".healthMinus img", $(tr)).attr("src", BASE_URL+"images/icons/honeypot/24/Rate_Thumbs_Down_BW.png");
		}
	}
	if (parseInt($("[name^=postHealth]", $(tr)).val()) >= 0)
	{
		$(".healthMinus img", $(tr)).attr("src", BASE_URL+"images/icons/commerce/24/Rate_Thumbs_Down.png");
		if (parseInt($("[name^=postHealth]", $(tr)).val()) > 0)
		{
			$(".healthPlus img", $(tr)).attr("src", BASE_URL+"images/icons/honeypot/24/Rate_Thumbs_Up_BW.png");
		}
	}
};
monitorHandler.proximityClick = function (event) {
	event = $.event.fix(event);
	if (event.preventDefault) event.preventDefault();
	
	var anchor = $(event.target);
	while ($(anchor).get(0).nodeName.toLowerCase() != "a")
	{
		anchor = $(anchor).parents("a");
	}
	
	var tr = $(event.target);
	while ($(tr).get(0).nodeName.toLowerCase() != "tr")
	{
		tr = $(tr).parents("tr");
	}
	
	if ($(anchor).hasClass("heartIcon"))
	{
		var src = BASE_URL+"images/icons/honeypot/24/Heart.png";
		
		var newval = Math.abs(parseInt($("[name^=postProximity]", $(tr)).val())-1);
		$("[name^=postProximity]", $(tr)).val(newval);
		
		if (newval == 0)
		{
			src = BASE_URL+"images/icons/honeypot/24/Heart_BW.png";
		}
		
		var flagTitle = newval == 1 ? TXT_POST_PROXIMITY_TRUE : TXT_POST_PROXIMITY_FALSE;

		$(".heartIcon img", $(tr)).attr("src", src);
		$.fn.tooltip("hide");
		$(".heartIcon", $(tr)).attr("title", flagTitle);
	}
};
monitorHandler.intensityClick = function (event) {
	event = $.event.fix(event);
	if (event.preventDefault) event.preventDefault();
	
	var anchor = $(event.target);
	while ($(anchor).get(0).nodeName.toLowerCase() != "a")
	{
		anchor = $(anchor).parents("a");
	}
	
	var tr = $(event.target);
	while ($(tr).get(0).nodeName.toLowerCase() != "tr")
	{
		tr = $(tr).parents("tr");
	}
	
	$(".intensityOK img", $(tr)).attr("src", BASE_URL+"images/icons/honeypot/24/Sun.png");
	$(".intensitySevere img", $(tr)).attr("src", BASE_URL+"images/icons/honeypot/24/Cloud.png");
	$(".intensityCritical img", $(tr)).attr("src", BASE_URL+"images/icons/honeypot/24/Rain.png");
	
	var newval = $("[name^=postIntensity]", $(tr)).val();
	
	var intensityDesatureateSelector = "";
	var intensityHighlightSelector = "";
	if ($(anchor).hasClass("intensityOK"))
	{
		newval = 1;
		intensityDesatureateSelector = ".intensitySevere img, .intensityCritical img";
		intensityHighlightSelector = ".intensityOK img";
		
		$(".intensityOK img", $(tr)).attr("src", BASE_URL+"images/icons/honeypot/24/Sun.png");
		$(".intensitySevere img", $(tr)).attr("src", BASE_URL+"images/icons/honeypot/24/Cloud_BW.png");
		$(".intensityCritical img", $(tr)).attr("src", BASE_URL+"images/icons/honeypot/24/Rain_BW.png");
	}
	else if ($(anchor).hasClass("intensitySevere"))
	{
		newval = 2;
		intensityDesatureateSelector = ".intensityOK img, .intensityCritical img";
		intensityHighlightSelector = ".intensitySevere img";
		
		$(".intensityOK img", $(tr)).attr("src", BASE_URL+"images/icons/honeypot/24/Sun_BW.png");
		$(".intensitySevere img", $(tr)).attr("src", BASE_URL+"images/icons/honeypot/24/Cloud.png");
		$(".intensityCritical img", $(tr)).attr("src", BASE_URL+"images/icons/honeypot/24/Rain_BW.png");
	}
	else if ($(anchor).hasClass("intensityCritical"))
	{
		newval = 3;
		intensityDesatureateSelector = ".intensityOK img, .intensitySevere img";
		intensityHighlightSelector = ".intensityCritical img";
		
		$(".intensityOK img", $(tr)).attr("src", BASE_URL+"images/icons/honeypot/24/Sun_BW.png");
		$(".intensitySevere img", $(tr)).attr("src", BASE_URL+"images/icons/honeypot/24/Cloud_BW.png");
		$(".intensityCritical img", $(tr)).attr("src", BASE_URL+"images/icons/honeypot/24/Rain.png");
	}
	
	$(intensityDesatureateSelector, $(tr)).css({border: 0})
	$(intensityHighlightSelector, $(tr)).css({border: "solid 1px #000"});
	
	$("[name^=postIntensity]", $(tr)).val(newval);
};
monitorHandler.topicAdd = function (event) {
	event = $.event.fix(event);
	if (event.preventDefault) event.preventDefault();
	
	var anchor = $(event.target);
	while ($(anchor).get(0).nodeName.toLowerCase() != "a")
	{
		anchor = $(anchor).parents("a");
	}
	
	var params = {};
	params.async = 0;
	params.url = BASE_URL+"overlays/topicHandler.php";
	params.data = {};
	$.fn.overlay("show", params);
	
	$("#save").bind("click", function() {
		monitorHandler.topicSave($(anchor), $("#frm_topicHandler").serializeJSON());
	});
};
monitorHandler.topicSave = function (anchor, formData) {
	var params = {};
	params.url = BASE_URL+"scripts/topicHandlerAJAX.php";
	params.data = formData;
	
	var tr = $(anchor).parents("tr");
	
	selectVals = [];
	$("[name^=topicID] option:selected").each(function () {
		selectVals[selectVals.length] = $(this).val();
	});
	
	var select = $("[name^=topicID]", $(tr));
	
	$.ajax({
		type: "POST",
		dataType: "json",
		async: true,
		url: params.url,
		cache: false,
		data: params.data,
		success: function (response) {
			var html = ""
			for (var i = 0; i < response.data.topics.length; i++)
			{
				html += "<option value=\""+response.data.topics[i].id+"\">"+response.data.topics[i].topicNameCZ+"</option>";
			}

			var i = 0;
			$("[name^=topicID]").each(function () {
				$(this).html($(html).clone());
				
				$(this).val(selectVals[i]);
				i++;
			});
			
			$(select).val(response.data.selectedID);
		}
	});
	
	$.fn.overlay("hide");
};
monitorHandler.hubAdd = function (event) {
	event = $.event.fix(event);
	if (event.preventDefault) event.preventDefault();
	
	insertedIDs = [];
	$(".monitoredHub").each(function () {
		insertedIDs[insertedIDs.length] = $(this).attr("id").substring(5, $(this).attr("id").length);
	});
	
	var params = {};
	params.async = 0;
	params.url = BASE_URL+"overlays/hubHandler.php";
	params.data = {
		insertedIDs: insertedIDs
	};
	$.fn.overlay("show", params);
	$.fn.tooltip("init");
	
	$(".tabs").tabs();
	
	$(".tabs").tabs({
		show: function(event, ui) {
			params = {};
			params.resize = "XY";
			$.fn.overlay("resize", params);
		}
	});
	
	$("#newHubURL").bind("keyup", monitorHandler.saveReset);
	$(".pickHub").bind("click", function (event) {
		event = $.event.fix(event);
		
		monitorHandler.hubPick(event);
	});
	$("#save").bind("click", function () {
		monitorHandler.hubSave($("#frm_hubHandler").serializeJSON());
	});
};
monitorHandler.hubPick = function (event) {
	event = $.event.fix(event);
	if (event.preventDefault) event.preventDefault();
	
	var tr = $(event.target);
	while ($(tr).get(0).nodeName.toLowerCase() != "tr")
	{
		tr = $(tr).parents("tr");
	}
	
	var param = {};
	param.id = $("[name=hubID]", $(tr)).val();
	param.hubName = $("[name=hubName]", $(tr)).val();
	param.hubURL = $("[name=hubURL]", $(tr)).val();
	
	param.hubURL = !param.hubURL.match(/http/gi) ? "http://"+param.hubURL : param.hubURL;
	
	monitorHandler.hubAppend(param);
	
	$.fn.overlay("hide");
};
monitorHandler.hubSave = function (formData) {
	var exists = false;
	
	if (!monitorHandler.postponedSave)
	{
		$.ajax({
			type: "POST",
			dataType: "json",
			async: false,
			url: BASE_URL+"scripts/checkURLExists.php",
			cache: false,
			data: {
				checkURL: formData.newHubURL,
				checkWhere: "hub"
			},
			success: function (response) {
				if (response.data && response.data.length > 0)
				{
					exists = true;
					
					$("<div>").attr("id", "existsInfo")
					.append(
						$("<p>").addClass("info")
						.html(TXT_PAGE_ALREADY_EXISTS)
					)
					.insertBefore($("#save"));
					
					$("#save").val(BTN_REALLY_SAVE)
					
					$("#show").removeClass("none")
					.bind("click", function () {
						$(".tabs").tabs("select", 1);
					});
				
					monitorHandler.postponedSave = true;
				
					params = {};
					params.resize = "XY";
					$.fn.overlay("resize", params);

					$("[name=hubID]", $("#frm_hubHandler")).each(function () {
						if (response.data.indexOf($(this).val()) != -1)
						{
							$(this).parents("tr").addClass("healthy");
						}
					});
				}
			}
		});
	}

	if (exists && monitorHandler.postponedSave)
	{
		return false;
	}
	
	var params = {};
	params.url = BASE_URL+"scripts/hubHandlerAJAX.php";
	params.data = formData;
	
	$.ajax({
		type: "POST",
		dataType: "json",
		async: true,
		url: params.url,
		cache: false,
		data: params.data,
		success: function (response) {
			var param = {};
			param.id = response.data.id;
			param.hubName = response.data.hubName;
			param.hubURL = response.data.hubURL;
			
			param.hubURL = !param.hubURL.match(/http/gi) ? "http://"+param.hubURL : param.hubURL;
			
			monitorHandler.hubAppend(param);
		}
	});
	
	monitorHandler.postponedSave = false;
	
	$.fn.overlay("hide");
};
monitorHandler.hubAppend = function (param) {
	$("<div>").attr("id", "hubID"+param.id)
	.addClass("monitoredHub")
	.append(
		$("<h3>")
		.append(
			$("<a>").append(
				$("<img>").attr("src", BASE_URL+"images/icons/information_management/24/Collapse.png")
			)
			.bind("click", monitorHandler.showHide)
		)
		.append(
			$("<a>").attr({
				href: param.hubURL,
				target: "_blank"
			})
			.text(param.hubName)
		)
		.append(
			$("<a>").attr({
				href: "#",
				title: TXT_PAGE_ADD
			})
			.addClass("pageAdd tooltip")
			.bind("click", monitorHandler.pageAdd)
			.append(
				$("<img>").attr("src", BASE_URL+"images/icons/information_management/24/Documents_x2_Add.png")
			)
		)
	)
	.appendTo($("#frm_monitoring"));
	
	$.fn.tooltip("init");
};
monitorHandler.pageAdd = function (event) {
	event = $.event.fix(event);
	if (event.preventDefault) event.preventDefault();
	
	insertedIDs = [];
	$(".monitoredPage").each(function () {
		insertedIDs[insertedIDs.length] = $(this).attr("id").substring(6, $(this).attr("id").length);
	});
	
	var hub = $(event.target).parents(".monitoredHub");
	
	var params = {};
	params.async = 0;
	params.url = BASE_URL+"overlays/pageHandler.php";
	params.data = {
		hubID: $(hub).attr("id").substring(5, $(hub).attr("id").length),
		insertedIDs: insertedIDs
	};
	$.fn.overlay("show", params);
	$.fn.tooltip("init");
	
	$(".tabs").tabs();
	
	$(".tabs").tabs({
		show: function(event, ui) {
			params = {};
			params.resize = "XY";
			$.fn.overlay("resize", params);
		}
	});
	
	$.fn.tooltip("init");
	
	$("#newPageURL").bind("keyup", monitorHandler.saveReset);
	$(".pickPage").bind("click", function (event) {
		event = $.event.fix(event);
		
		monitorHandler.pagePick(event);
	});
	$("#save").bind("click", function () {
		monitorHandler.pageSave($("#frm_pageHandler").serializeJSON());
	});
};
monitorHandler.pagePick = function (event) {
	event = $.event.fix(event);
	if (event.preventDefault) event.preventDefault();
	
	var tr = $(event.target);
	while ($(tr).get(0).nodeName.toLowerCase() != "tr")
	{
		tr = $(tr).parents("tr");
	}
	
	var param = {};
	param.hubID = $("[name=pickHubID]", $(tr)).val();
	param.pageID = $("[name=pageID]", $(tr)).val();
	param.pageURL = $("[name=pageURL]", $(tr)).val();
	
	param.pageURL = !param.pageURL.match(/http/gi) ? "http://"+param.pageURL : param.pageURL;
	
	monitorHandler.pageAppend(param);
	
	$.fn.overlay("hide");
};
monitorHandler.pageSave = function (formData) {
	var exists = false;
	
	if (!monitorHandler.postponedSave)
	{
		$.ajax({
			type: "POST",
			dataType: "json",
			async: false,
			url: BASE_URL+"scripts/checkURLExists.php",
			cache: false,
			data: {
				checkURL: formData.newPageURL,
				checkWhere: "page"
			},
			success: function (response) {
				if (response.data && response.data.length > 0)
				{
					exists = true;
					
					$("<div>").attr("id", "existsInfo")
					.append(
						$("<p>").addClass("info")
						.html(TXT_PAGE_ALREADY_EXISTS)
					)
					.insertBefore($("#save"));
					
					$("#save").val(BTN_REALLY_SAVE)
					
					$("#show").removeClass("none")
					.bind("click", function () {
						$(".tabs").tabs("select", 1);
					});
				
					monitorHandler.postponedSave = true;
				
					params = {};
					params.resize = "XY";
					$.fn.overlay("resize", params);

					$("[name=pageID]", $("#frm_pageHandler")).each(function () {
						if (response.data.indexOf($(this).val()) != -1)
						{
							$(this).parents("tr").addClass("healthy");
						}
					});
				}
			}
		});
	}

	if (exists && monitorHandler.postponedSave)
	{
		return false;
	}

	var params = {};
	params.url = BASE_URL+"scripts/pageHandlerAJAX.php";
	params.data = formData;
	
	$.ajax({
		type: "POST",
		dataType: "json",
		async: true,
		url: params.url,
		cache: false,
		data: params.data,
		success: function (response) {
			var param = {};
			param.hubID = response.data.hubID;
			param.pageID = response.data.pageID;
			param.pageURL = response.data.pageURL;
			
			param.pageURL = !param.pageURL.match(/http/gi) ? "http://"+param.pageURL : param.pageURL;
			
			monitorHandler.pageAppend(param);
		}
	});
	
	monitorHandler.postponedSave = false;
	
	$.fn.overlay("hide");
};
monitorHandler.pageAppend = function (param) {
	var displayText = param.pageURL.substring(0, 100);
	displayText += param.pageURL.length > 100 ? "..." : "";
	
	$("<div>").attr("id", "pageID"+param.pageID)
	.addClass("monitoredPage")
	.append(
		$("<h5>")
		.append(
			$("<a>").append(
				$("<img>").attr("src", BASE_URL+"images/icons/information_management/24/Collapse.png")
			)
			.bind("click", monitorHandler.showHide)
		)
		.append(
			$("<a>").attr({
				href: param.pageURL,
				target: "_blank"
			})
			.text(displayText)
		)
		.append(
			$("<input>").attr({
				type: "text",
				id: "sessionPage"+param.pageID,
				name: "sessionPage"+param.pageID,
				size: 2
			}).val(1)
		)
		.append("&nbsp;")
		.append(
			$("<a>").attr({
				href: "#",
				title: TXT_SESSION_ADD
			})
			.addClass("sessionAdd tooltip")
			.append(
				$("<img>").attr("src", BASE_URL+"images/icons/basic/24/Comment_Add.png")
			)
			.bind("click", monitorHandler.sessionAdd)
		)
		.append("&nbsp;")
		.append(
			$("<a>").attr({
				href: "#",
				title: TXT_POST_GROUP_ADD
			})
			.addClass("postsAdd tooltip")
			.append(
				$("<img>").attr("src", BASE_URL+"images/icons/information_management/24/Conversation_Add.png")
			)
			.bind("click", monitorHandler.postsAdd)
		)
		/*.append(
			$("<a>").attr("href", "#")
			.addClass("pageAdd")
			.bind("click", monitorHandler.pageAdd)
			.append(
				$("<img>").attr("src", BASE_URL+"images/icons/information_management/24/Documents_x2_Add.png")
			)
		)*/
	)
	.appendTo($("#hubID"+param.hubID));
	
	$.fn.tooltip("init");
};
monitorHandler.postsAdd = function (event) {
	monitorHandler.sessionAdd(event, 1);
};
monitorHandler.sessionAdd = function (event, grouped) {
	event = $.event.fix(event);
	if (event.preventDefault) event.preventDefault();
	
	var grouped = grouped == 1 ? 1 : 0;
	var groupedClass = "";
	
	var rowsAdd = 1;
	
	var page = $(event.target).parents(".monitoredPage");
	if (parseInt($("input[name^=sessionPage]", $(page)).val()) > 0)
	{
		rowsAdd = parseInt($("input[name^=sessionPage]", $(page)).val());
	}
	
	if (grouped)
	{
		rowsAdd = 1;
		
		groupedClass = " class=\"grouped\"";
		var countElem = $("<input>").addClass("required tooltip")
		.attr({
			type: "text",
			size: 3,
			name: "postCount[]",
			title: TXT_POST_GROUP_COUNT
		});
	}
	else
	{
		var countElem = $("<input>")
		.attr({
			type: "hidden",
			name: "postCount[]",
			value: 1
		});
	}

	var canPostText = false;
	$.ajax({
		type: "POST",
		dataType: "json",
		async: false,
		url: BASE_URL+"scripts/checkUserPrivilege.php",
		cache: false,
		data: {
			userID: $("#userID").val(),
			canDo: "postText"
		},
		success: function (response) {
			canPostText = response.data.hasPrivilege;
		}
	});
	
	var data = {};
	data.hubID = $(page).parents(".monitoredHub").attr("id").substring(5, $(page).parents(".monitoredHub").attr("id").length);
	data.pageID = $(page).attr("id").substring(6, $(page).attr("id").length);
	
	var html = "<table width=\"100%\" class=\"simpleTable\"><thead><tr><th width=\"40\">&nbsp;</th><th width=\"100\">"+TXT_POST_DATE+"</th><th width=\"50\">"+TXT_POST_TIME+"</th><th colspan=\"3\" class=\"borderRight\">"+TXT_POST_HEALTH+"</th><th width=\"30\" class=\"borderRight\">"+TXT_POST_PROXIMITY+"</th><th colspan=\"3\">"+TXT_POST_INTENSITY+"</th><th>"+TXT_POST_TOPIC+"/"+TXT_POST_INSERTED_BY+"</th>";
	html += "<th>"
	if (canPostText)
	{
		html += TXT_POST_TEXT;
	}
	html += "</th>"
	html += "<th width=\"24\">&nbsp;</th></tr></thead><tbody></tbody></table>";
	
	$("<div>").attr("id", "sessionIDX"+($(".monitoredSession").length))
	.addClass("monitoredSession")
	.html(html)
	.appendTo($(page));
	
	$(".monitoredSession:last th:first", $(page)).append(
		$("<a>").append(
			$("<img>").attr("src", BASE_URL+"images/icons/information_management/16/Collapse.png")
		)
		.bind("click", monitorHandler.showHide)
	);
	
	var re = new RegExp("\<%BASE_URL%\>", "g");
	
	var topicsHTML = "";
	$.ajax({
		type: "POST",
		dataType: "json",
		async: false,
		url: BASE_URL+"scripts/topicGetAJAX.php",
		cache: false,
		data: {},
		success: function (response) {
			for (var i = 0; i < response.data.length; i++)
			{
				topicsHTML += "<option value=\""+response.data[i].id+"\">"+response.data[i].topicNameCZ+"</option>";
			}
		}
	});
	
	var tmpLength = 0;
	$("[name^=postCount]", $(page)).each(function () {
		tmpLength += parseInt($(this).val());
	});
	var tmpLengthAll = $(".monitoredSession tbody tr").length;
	$.get(BASE_URL+"pages/postTemplate.php", function (response) {
		var tmpDate = new Date();
		var tmpToday = tmpDate.getUTCFullYear()+"-"+(tmpDate.getUTCMonth()+1)+"-"+tmpDate.getUTCDate();
		var j = 0;
		for (var i = 0; i < rowsAdd; i++)
		{
			var substForI = tmpLength+i+1;
			var inputType = "hidden";
			var dateFromRequired = false;
			var dateFromValue = "";
			var dateBR = "";
			if (grouped)
			{
				substForI = "";
				inputType = "text";
				dateFromRequired = " required";
				dateFromValue = date_handler.convert(tmpToday, "Y-m-d", DATE_FORMAT);
				dateBR = "<br />";
			}
			
			substForI += $('<div>').append($(countElem).clone()).remove().html();
			
			var html = response;
			html = html.replace("<%GROUP_CLASS%>", groupedClass);
			html = html.replace("<%I%>", substForI);
			html = html.replace("<%J%>", tmpLengthAll+i+1);
			html = html.replace("<%J%>", tmpLengthAll+i+1);
			html = html.replace("<%MD5CHECKSUM%>", "");
			html = html.replace("<%HUB_ID%>", data.hubID);
			html = html.replace("<%PAGE_ID%>", data.pageID);
			html = html.replace("<%POST_ID%>", "");
			html = html.replace("<%POST_HEALTH%>", 0);
			html = html.replace("<%POST_PROXIMITY%>", 0);
			html = html.replace("<%POST_INTENSITY%>", 1);
			html = html.replace("<%HEART_TITLE%>", TXT_POST_PROXIMITY_FALSE);
			html = html.replace("<%INTENSITY_OK%>", TXT_POST_INTENSITY_OK);
			html = html.replace("<%INTENSITY_SEVERE%>", TXT_POST_INTENSITY_SEVERE);
			html = html.replace("<%INTENSITY_CRITICAL%>", TXT_POST_INTENSITY_CRITICAL);
			html = html.replace(re, BASE_URL);
			html = html.replace("<%FLAG%>", "Emoticon_Tense.png");
			html = html.replace("<%DATE_FROM_HIDDEN%>", inputType);
			html = html.replace("<%DATE_FROM_HIDDEN%>", inputType);
			html = html.replace("<%DATE_FROM_VALUE%>", dateFromValue);
			html = html.replace("<%DATE_FROM_REQUIRED%>", dateFromRequired);
			html = html.replace("<%DATE_FROM_REQUIRED%>", dateFromRequired);
			html = html.replace("<%TIME_FROM_VALUE%>", "");
			html = html.replace("<%DATE_BR%>", dateBR);
			html = html.replace("<%DATE_BR%>", dateBR);
			html = html.replace("<%TOOLTIP_DATE%>", TXT_MONITORING_DATE);
			html = html.replace("<%TOOLTIP_TIME%>", TXT_MONITORING_TIME);
			html = html.replace("<%DATE_AT%>", date_handler.convert(tmpToday, "Y-m-d", DATE_FORMAT));
			html = html.replace("<%TIME_AT%>", "");
			html = html.replace("<%TOPICS%>", topicsHTML);
			html = html.replace("<%POSTED_BY_USER%>", "");
			if (!canPostText)
			{
				html = html.replace("<%TEXT_TD%>", "<td><input type=\"hidden\" name=\"postBody[]\" />");
			}
			else
			{
				if (!grouped)
				{
					html = html.replace("<%TEXT_TD%>", "<td><div class=\"postBodyContainer\"><textarea name=\"postBody[]\" rows=\"3\" cols=\"48\"></textarea></div></td>");
				}
				else
				{
					html = html.replace("<%TEXT_TD%>", "<td><input type=\"hidden\" name=\"postBody[]\" />");
				}
			}
			html = html.replace("<%THUMBS_UP%>", TXT_POST_THUMB_UP);
			html = html.replace("<%THUMBS_DOWN%>", TXT_POST_THUMB_DOWN);
			html = html.replace("<%FLAG_EXPLAIN%>", TXT_POST_HEALTH+": "+TXT_POST_FLAG_BLUE);
			html = html.replace("<%TOPIC_ADD%>", TXT_POST_TOPIC_ADD);
			html = html.replace("<%DELETE_A%>", "<a href=\"#\" class=\"deletePost\"><img src=\""+BASE_URL+"images/icons/basic/24/Delete.png\" /></a>");
			html = html.replace("<%RATE_UP_IMG%>", BASE_URL+"images/icons/commerce/24/Rate_Thumbs_Up.png");
			html = html.replace("<%RATE_DOWN_IMG%>", BASE_URL+"images/icons/commerce/24/Rate_Thumbs_Down.png");
			html = html.replace("<%HEART_IMG%>", "Heart_BW");
			html = html.replace("<%SUN_IMG%>", "Sun");
			html = html.replace("<%SUN_STYLE%>", "");
			html = html.replace("<%CLOUD_IMG%>", "Cloud_BW");
			html = html.replace("<%CLOUD_STYLE%>", "");
			html = html.replace("<%RAIN_IMG%>", "Rain_BW");
			html = html.replace("<%RAIN_STYLE%>", "");
			
			$(".monitoredSession table:last tbody", $(page)).append(html);
		}
		
		$(".monitoredSession:last .healthPlus, .monitoredSession:last .healthMinus", $(page)).bind("click", monitorHandler.healthClick);
		$(".monitoredSession:last .topicAdd", $(page)).bind("click", monitorHandler.topicAdd);
		$(".monitoredSession:last .deletePost", $(page)).bind("click", monitorHandler.postDelete);
		$(".monitoredSession:last .heartIcon", $(page)).bind("click", monitorHandler.proximityClick);
		$(".monitoredSession:last .intensityOK, .monitoredSession:last .intensitySevere, .monitoredSession:last .intensityCritical", $(page)).bind("click", monitorHandler.intensityClick);
		
		$(".monitoredSession:last .intensityOK img", $(page)).css({border: "solid 1px #000"});
			
		$.fn.tooltip("init");
		if (typeof(form_handler) != "undefined" && typeof(form_handler.init) != "undefined") form_handler.init();
		if (typeof(calendar) != "undefined" && typeof(calendar.init) != "undefined") calendar.init();
		
		if (grouped)
		{
			$(".monitoredSession:last input[name^=postCount]:first", $(page)).focus();
		}
		else
		{
			$(".monitoredSession:last input[name^=timeAt]:first", $(page)).focus();
		}
		
		$("input[name^=timeAt]").bind("blur", function () {
			var newval = $(this).val();
			
			if (newval.length <= 0)
			{
				return;
			}
			
			var newvalSplit = newval.split(/[^\d]+/);
			if (newvalSplit.length > 1)
			{
				newvalSplit[0] = new Number(newvalSplit[0]).valueOf();
				newvalSplit[1] = new Number(newvalSplit[1]).valueOf();
				
				newvalSplit[0] = newvalSplit[0] >= 24 ? 0 : newvalSplit[0];
				newvalSplit[1] = newvalSplit[1] > 59 ? 59 : newvalSplit[1];

				newval = sprintf("%02d", parseInt(newvalSplit[0]));
				newval += ":";
				if (newvalSplit[1] < 6)
				{
					newval += (newvalSplit[1]+"0");
				}
				else
				{
					newval += sprintf("%02d", parseInt(newvalSplit[1]));
				}
			}
			else
			{
				switch (newval.length)
				{
					case 0:
						newval = newval;
						break;
					case 1:
						newval = sprintf("%02d:00", parseInt(newval));
						break;
					case 2:
						newval = parseInt(newval) >= 24 ? 0 : newval;
						newval = sprintf("%02d:00", parseInt(newval));
						break;
					case 3:
						var tmp = newval;
						var tmpArr = [];
						tmpArr[0] = tmp[0];
						tmpArr[1] = tmp.substring(1, 3);
						tmpArr[0] = parseInt(tmpArr[0]) > 24 ? 0 : tmpArr[0];
						tmpArr[1] = parseInt(tmpArr[1]) > 59 ? 59 : tmpArr[1];
						
						newval = sprintf("%02d", parseInt(tmpArr[0]));
						newval += ":";
						newval += sprintf("%02d", parseInt(tmpArr[1]));
						break;
					case 4:
					default:
						var tmp = newval;
						var tmpArr = [];
						tmpArr[0] = tmp.substring(0, 2);
						tmpArr[1] = tmp.substring(2, 4);
						tmpArr[0] = parseInt(tmpArr[0]) >= 24 ? 0 : tmpArr[0];
						tmpArr[1] = parseInt(tmpArr[1]) > 59 ? 59 : tmpArr[1];
						
						newval = sprintf("%02d", parseInt(tmpArr[0]));
						newval += ":";
						newval += sprintf("%02d", parseInt(tmpArr[1]));
						break;
				}
			}
		
			$(this).val(newval);
			form_handler.checkField(this);
		});
	});
};
monitorHandler.postDelete = function (event) {
	event = $.event.fix(event);
	if (event.preventDefault) event.preventDefault();
	
	if ($("tr", $(event.target).parents("tbody")).length == 1)
	{
		$("tr", $(event.target).parents(".monitoredSession")).remove();
	}
	else
	{
		$(event.target).parents("tr").remove();
	}
};
monitorHandler.saveReset = function () {
	monitorHandler.postponedSave = false;
	
	$("#save").val(BTN_SAVE)
					
	$("#show").removeClass("none")
	.addClass("none")
	.unbind();
	
	$("#existsInfo").remove();
	
	$("#overlayContent tr.healthy").removeClass("healthy");
	
	params = {};
	params.resize = "XY";
	$.fn.overlay("resize", params);
};
monitorHandler.notifyAdmins = function (event) {
	event = $.event.fix(event);
	if (event.preventDefault) event.preventDefault();
	
	var params = {};
	params.async = 0;
	params.url = BASE_URL+"overlays/notifyAdmins.php";
	params.data = {};
	$.fn.overlay("show", params);
	
	$("#save").bind("click", function () {
		$.ajax({
			type: "POST",
			dataType: "json",
			async: true,
			url: BASE_URL+"scripts/notifyAdmins.php",
			cache: false,
			data: $("#frm_notifyAdmins").serializeJSON(),
			success: function (response) {
				if (response.status == "ok")
				{
					$.fn.statusBar("fadeWith", {html: JS_TXT_NOTIFY_OK});
				}
				else
				{
					$.fn.statusBar("showWith", {html: JS_TXT_NOTIFY_FAILED});
				}
			}
		});
	});
};

//////////////////
monitorHandler.showHide = function (event) {
	if (event.nodeType == 1)
	{
		element = $(event).parents(".monitoredPage");
	}
	else
	{
		event = $.event.fix(event);
	
		var anchor = $(event.target);
		while ($(anchor).get(0).nodeName.toLowerCase() != "a")
		{
			anchor = $(anchor).parents("a");
		}
		
		var element = $(anchor).parent().parent(".monitoredHub");
		if ($(element).length <= 0)
		{
			element = $(anchor).parent().parent(".monitoredPage");
			if ($(element).length <= 0)
			{
				element = $(anchor).parent().parent(".monitoredSession");
			}
		}
	}
	
	var re = new RegExp("Collapse");
	if (event.nodeType != 1)
	{
		if ($(element).hasClass("monitoredSession"))
		{
			$("table tbody", $(element)).toggle();
		}
		else if ($(element).hasClass("monitoredPage"))
		{
			$(".monitoredSession", $(element)).toggle();
		}
		else
		{
			$(".pagesContainer", $(element)).toggle();
		}
		
		if ($("img", $(anchor)).attr("src").match(re))
		{
			$(".loadMore", $(element)).remove();
			$("img", $(anchor)).attr("src", $("img", $(anchor)).attr("src").replace("Collapse", "Expand"));
		}
		else
		{
			$("img", $(anchor)).attr("src", $("img", $(anchor)).attr("src").replace("Expand", "Collapse"));
		}
	}
	
	if (event.nodeType == 1 || $("img", $(anchor)).attr("src").match(re))
	{
		if ($(element).hasClass("monitoredPage"))
		{
			var params = {};
			params.url = BASE_URL+"scripts/getPosts.php";
			params.data = {
				pageID: $(element).attr("id").substring(6, $(element).attr("id").length)
			};
				
			var postCountSum = 0;
			$("input[name^=postCount]", $(element)).each(function () {
				postCountSum += parseInt($(this).val());
			});
				
			if (event.nodeType == 1)
			{
				$.extend(params.data, {
					offset: $(event).attr("rel"),
					postCountOffset: parseInt(postCountSum)
				});
			}

			$.ajax({
				type: "POST",
				dataType: "html",
				async: true,
				url: params.url,
				cache: false,
				data: params.data,
				success: function (response) {
					if (event.nodeType != 1)
					{
						$(".monitoredSession", $(element)).remove();
					}
					$(".loadMore", $(element)).remove();
					$(element).append(response);

					if (typeof(form_handler) != "undefined" && typeof(form_handler.init) != "undefined") form_handler.init();
					if (typeof(calendar) != "undefined" && typeof(calendar.init) != "undefined") calendar.init();
					
					monitorHandler.initPosts();
						
					$.fn.tooltip("init");
						
					if (event.nodeType == 1)
					{
						var tmpScrollStart = parseInt($("#monitoringWorkspace").scrollTop());
						var tmpScrollEnd = parseInt($("#monitoringWorkspace").scrollTop()+$(".monitoredSession:last", $(element)).position().top-$("#monitoringWorkspace").position().top);
						
						$("#monitoringWorkspace").scrollTop(tmpScrollEnd);
						
						/*var tmpSleep = 3000/(tmpScrollEnd-tmpScrollStart);
						
						for (var i = tmpScrollStart; i <= tmpScrollEnd; i+=10)
						{
							setTimeout(function () { $("#monitoringWorkspace").scrollTop(i) }, tmpSleep);
						}*/
					}
				}
			});
		}
	}
};

$(document).ready(function () {
	monitorHandler.init();
	
	$(".monitoredHub h3 a:first-child").each(function () {
		$(this).bind("click", monitorHandler.showHide);
	});
	
	$(".monitoredPage h5 a:first-child").each(function () {
		$(this).bind("click", monitorHandler.showHide);
	});
	
	$("#monitoringWorkspace").height(($(window).height()-$("#work").offset().top-$("#monitoringControls").outerHeight(true)-($("#work").offset().top-$("#main").offset().top)*3-4)+"px");
});