// JavaScript Document
var tempdir = '';



$(window).load(function() {
  // Animate loader off screen
  $(".se-pre-con").fadeOut("slow");;
});

$(document).ready(function () {
 
  slideShow();
   menu();
  startmenu();
/*
   header_elm = document.getElementById('slideshow'); 
   header_dimmer = document.getElementById('content');
    if(header_elm.getElementsByTagName('article')){
    header_title = header_elm.getElementsByTagName('article')[0];
    }
*/
   if(document.getElementById('ctr')){
   document.getElementById('ctr').value = "none";
   }
  	$("#menu").sticky({topSpacing: -18});
  //	$("#content-right").sticky({topSpacing: 48});
    

  if (document.getElementById('dotazform')) {

        document.querySelector("form")
            .addEventListener("invalid", function (event) {
                event.preventDefault();
            }, true);


        function replaceValidationUI(form) {
            // Suppress the default bubbles
            form.addEventListener("invalid", function (event) {
                event.preventDefault();
            }, true);

            // Support Safari, iOS Safari, and the Android browser each of which do not prevent
            // form submissions by default
            form.addEventListener("submit", function (event) {
                if (!this.checkValidity()) {
                    event.preventDefault();
                }
            });

            var submitButton = form.querySelector("button:not([type=button]), input[type=submit]");
            submitButton.addEventListener("click", function (event) {
                var invalidFields = form.querySelectorAll(":invalid"),
                    errorMessages = form.querySelectorAll(".error-message"),
                    parent;

                // Remove any existing messages
                for (var i = 0; i < errorMessages.length; i++) {
                     errorMessages[i].parentNode.removeChild(errorMessages[i]);
                }

                for (var i = 0; i < invalidFields.length; i++) {
                    parent = invalidFields[i].parentNode;
                    parent.insertAdjacentHTML("beforeend", "<div class='error-message'>" +
                        invalidFields[i].validationMessage +
                        "</div>");
                    $('.error-message').animate({ opacity: '1' }, 500);
                }

                // If there are errors, give focus to the first invalid field
                if (invalidFields.length > 0) {
                     
                    invalidFields[1].focus();
                }
            });
        }

        // Replace the validation UI for all forms
        var forms = document.querySelectorAll("form");
        for (var i = 0; i < forms.length; i++) {
            replaceValidationUI(forms[i]);
        }

    }


});


function startmenu() {
  if (document.getElementById('startmenu')) {
    var startmenu = document.getElementById('startmenu');
    startmenu.onclick = showRespMenu;
  }
}
function showRespMenu() {

  if ($('#mini-menu').css("display") == "block") {
    $('#mini-menu').animate({ width: 0 + '%' }, 100);
    $('#mini-menu').css({ display: 'none' });
    $('#mini-menu li').css({ display: 'none' });
  }
  else {
    $('#mini-menu').animate({ width: '350px' }, 100);
    $('#mini-menu').css({ display: 'block' });
    $('#mini-menu li').css({ display: 'block' });
  }

}

function menu() {
   
  var nav = document.getElementById('menu');
  var as = nav.getElementsByTagName('a');
  for (var i = 0; i < as.length; i++) {
    if (as[i].parentNode.tagName == 'LI') {
      
      var span = as[i].parentNode.getElementsByTagName('SPAN');
      for (var y = 0; y < span.length; y++) {
        if (span[y].getElementsByTagName('A').length > 0) {
            
          as[i].number = i;
          as[i].onclick = test2;

        }
      }
    }
  }
}

/*
listen = function (element, event, callback) {

    if (element.addEventListener) {
        element.addEventListener(event, callback, false);
    } else if (element.attachEvent) {
        element.attachEvent(event, callback);
    }
};

var header_elm;
var header_dimmer;
var header_title;

    listen(window, 'scroll', function (event) {
        
        if (window.requestAnimationFrame) {
            window.requestAnimationFrame(function () {
                handleScroll.call(this, event);
            });
        } else if (window.webkitRequestAnimationFrame) {
            window.webkitRequestAnimationFrame(function () {
                handleScroll.call(this, event);
            });
        } else if (window.mozRequestAnimationFrame) {
            window.mozRequestAnimationFrame(function () {
                handleScroll.call(this, event);
            });
        } else if (window.msRequestAnimationFrame) {
            window.msRequestAnimationFrame(function () {
                handleScroll.call(this, event);
            });
        } else if (window.oRequestAnimationFrame) {
            window.oRequestAnimationFrame(function () {
                handleScroll.call(this, event);
            });
        }
    });


    handleScroll = function (event) {
 
    if (document.body.offsetWidth > 550) {
 
        var header_height = header_elm.offsetHeight;

      

        var scroll = document.documentElement.scrollTop;
        var scroll2 = document.body.scrollTop;
        scroll = scroll2 > scroll ? scroll2 : scroll;
        if (scroll > header_height) return;
       // header_dimmer.style.opacity = scroll / header_height * 0.9;
        var scale = 1 + scroll / header_height * 0.2;
        header_elm.style['transform'] = 'scale(' + scale + ')';
        header_elm.style['-o-transform'] = 'scale(' + scale + ')';
        header_elm.style['-webkit-transform'] = 'scale(' + scale + ')';
        header_elm.style['-moz-transform'] = 'scale(' + scale + ')';
        header_elm.style.opacity = 1 / (scroll / header_height ) * 0.4;
        header_title.style.opacity = 1 / (scroll / header_height ) * 0.05;
       // nav.style.opacity = 1 / (scroll / header_height ) * 0.05;
       // closeHeaderVideo.style.opacity = 1 / (scroll / header_height ) * 0.05;
        //playHeaderVideo.style.opacity = 1 / (scroll / header_height ) * 0.05;
        // document.getElementById("scrollhere").innerHTML = scroll+ ' - ';

        if (scroll < 15) {
            header_elm.style['transform'] = 'scale(1)';
            header_elm.style['-o-transform'] = 'scale(1)';
            header_elm.style['-webkit-transform'] = 'scale(1)';
            header_elm.style['-moz-transform'] = 'scale(1)';
            header_elm.style.opacity = 1;
            header_title.style.opacity = 1;
           // nav.style.opacity = 1;
           // closeHeaderVideo.style.opacity = 1;
           // playHeaderVideo.style.opacity = 1;
            //document.getElementById("scrollhere").innerHTML = scroll;
        }
    }
}; 


*/

function slideShow() {
  if ($("#slideshow").length <= 0) {
    
    return;
  }
  if ($(".slide").length <= 1) {
    
    return;
  }



  //$('#slideshow div').css({opacity: 0.0; right: -10000px});	
  $('#slideshow .slide').css({ left: -10000 + 'px' });
  $('#slideshow div:first').css({ left: 0 + 'px' });
  $('#slideshow div:first').css({ opacity: 1.0 });
  $('#slideshow .caption').css({ opacity: 1 });
  $('#slideshowmover').click(gallery);
  $('#slideshowleftmover').click(galleryback);
  intervalID = setInterval('gallery()', 10000);

}

function gallery() {

   var mycap =  document.getElementById('slideshow'); 
   var newheight = mycap.offsetHeight  ;
   var newwidth = mycap.offsetWidth  ;   
  var current = ($('#slideshow div.show') ? $('#slideshow div.show') : $('#slideshow div:first'));
  var next = ((current.next().length) ? ((current.next().hasClass('non')) ? $('#slideshow div:first') : current.next()) : $('#slideshow div:first'));
	// var description = current.next('.description') ;	           
	next.css({left: newwidth})
	next.css({opacity: 0})
  .addClass('show')
	next.animate({left: '0px'}, 1)	  
  next.animate({opacity: 1}, 2000)
  current.animate({opacity: 0}, 2000)	
 	current.animate({left: -newwidth}, 1)	
	.removeClass('show');
  clearInterval(intervalID);
  intervalID = setInterval('gallery()', 10000);
  if ($('#my1').attr("class") == 'slide first show') { $('#myem1').css({ background: '#008dd2' }); } else { $('#myem1').css({ background: '#fff' }); }
  for (i = 2; i < ($("#slideshow div").length + 1); i++) {
    var my = '#my' + i;
    var myem = '#myem' + i;
    if ($(my).attr("class") == 'slide show') { $(myem).css({ background: '#008dd2' }); } else { $(myem).css({ background: '#fff' }); }
  }
}

function galleryback() {
  var mycap = document.getElementById('slideshow');
  var newheight = mycap.offsetHeight;
  var newwidth = mycap.offsetWidth;
  var current = ($('#slideshow div.show') ? $('#slideshow div.show') : $('#slideshow div:last'));
  var prev = ((current.prev().length) ? ((current.prev().hasClass('non')) ? $('#slideshow div:last') : current.prev()) : $('#slideshow div:last'));
  //var caption = next.attr('rel');	           
  prev.css({ left: -newwidth })
  /*
  prev.css({ opacity: 1 })
    .addClass('show')
  prev.animate({ left: '0px' }, 500)
  current.animate({ left: newwidth }, 500)
    .removeClass('show');
  */
  prev.css({opacity: 0})
  .addClass('show')
	prev.animate({left: '0px'}, 1)	  
  prev.animate({opacity: 1}, 2000)
  current.animate({opacity: 0}, 2000)	
 	current.animate({left: newwidth}, 500)	
	.removeClass('show');
  clearInterval(intervalID);
  intervalID = setInterval('gallery()', 10000);
  if ($('#my1').attr("class") == 'slide first show') { $('#myem1').css({ background: '#008dd2' }); } else { $('#myem1').css({ background: '#fff' }); }
  for (i = 2; i < ($("#slideshow div").length + 1); i++) {
    var my = '#my' + i;
    var myem = '#myem' + i;
    if ($(my).attr("class") == 'slide show') { $(myem).css({ background: '#008dd2' }); } else { $(myem).css({ background: '#fff' }); }
  }
}



function test2(nr, direction) {
  
  

  if (nr >= 0)
  { this.number = nr; }
  else {
    var x = this.number;
    slideExcept2(this.number);
  }
  var nav = document.getElementById('menu');
  var as = nav.getElementsByTagName('a');
  var spans = as[this.number].parentNode.getElementsByTagName('SPAN');
  var aheights = spans[0].getElementsByTagName('a');

  //pokud v html receno, ze ma byt zobrazen
  if (spans[0].className == "opened") {
    vyska = (aheights.length * 2.25);
    if (spans[0].style.height == "") { spans[0].style.height = vyska + "em"; }
    spans[0].style.display = 'block';
    spans[0].style.overflow = 'hidden';


    //pokud kliknuto na uz zobrazeny - zaviram
    if (parseFloat(spans[0].style.height) > 0.2) {

      spans[0].style.height = parseInt(spans[0].style.height) - 1 + "em";
      nr = this.number;
      direction = 0;
      setTimeout("test2(" + nr + "," + direction + ")", 10);
    }
    else {
      spans[0].style.display = 'none';
      spans[0].style.height = 0;
      spans[0].className = "none";

    }


  }

  // pokud neni ovlivnen html
  else {

    if (spans[0].style.display == "") { spans[0].style.display = "none"; }
    if (spans[0].style.height == "") { spans[0].style.height = 0 + "em"; }


    //pokud je zavreny - oteviram
    if ((spans[0].style.display == "none") || (direction == 1)) {

      spans[0].style.display = 'block';
      spans[0].style.overflow = 'hidden';
      as[this.number].className = 'click';
      var vyska = (aheights.length * 2.25);

 

      // neni otevren uplne

      if (parseFloat(spans[0].style.height) < vyska) {

        spans[0].style.height = (parseFloat(spans[0].style.height) + 1) + "em";
        nr = this.number;
        direction = 1;
        setTimeout("test2(" + nr + "," + direction + ")", 10);

      }
      // uz je otevrenej dost  
      else {
        spans[0].style.height = vyska + "em";
      }
    }

    //pokud je otevreny - zaviram
    else {

      if (parseInt(spans[0].style.height) > 0) {
        var ems = spans[0].getElementsByTagName('em');

        for (var e = 0; e < ems.length; e++) {
          ems[e].style.display = 'none';
        }
        spans[0].style.height = parseFloat(spans[0].style.height) - 1 + "em";
        as[this.number].className = 'noclick';
        nr = this.number;
        direction = 0;
        setTimeout("test2(" + nr + "," + direction + ")", 10);
      }
      else {

        // uplne schovam
        spans[0].style.display = 'none';
        spans[0].style.height = 0;
        as[this.number].className = 'noclick';
      }
    }
  }
 
  return false;

}



function slideExcept2(x) {

  var nav = document.getElementById('menu');
  var as = nav.getElementsByTagName('a');

  for (var i = 0; i < (as.length); i++) {

    if (as[i].parentNode.tagName == 'SPAN') {
      as[i].className = 'noclick';
    }

    if (i == x) {
    }
    else {
      var spans = as[i].parentNode.getElementsByTagName('SPAN');

      for (var w = 0; w < spans.length; w++) {
        var ems = spans[w].getElementsByTagName('em');
        for (var e = 0; e < ems.length; e++) {
          ems[e].style.display = 'none';
          ems[e].parentNode.style.marginBottom = 0 + 'em';
          var parenta = ems[e].parentNode.getElementsByTagName('a');
          for (var p = 0; p < parenta.length; p++) {
            parenta[p].className = 'back';
          }
        }
        if (parseInt(spans[0].style.height) > 0) {
          spans[0].style.display = 'none';
          spans[0].style.height = 0;

        }
      }
    }
  }

}


function startmenu() {
  if (document.getElementById('startmenu')) {
    var startmenu = document.getElementById('startmenu');
    startmenu.onclick = showRespMenu;
  }
}
function showRespMenu() {

  if ($('#mini-menu').css("display") == "block") {
    $('#mini-menu').animate({ width: 0 + '%' }, 100);
    $('#mini-menu').css({ display: 'none' });
    $('#mini-menu li').css({ display: 'none' });
  }
  else {
    $('#mini-menu').animate({ width: '350px' }, 100);
    $('#mini-menu').css({ display: 'block' });
    $('#mini-menu li').css({ display: 'block' });
  }

}



 function goWrite( nr ){
        var terms = document.getElementById("terms");
        var inputs =  terms.getElementsByTagName("input");
        document.getElementById("checkterms").value = ''; 
        for (var i = 0; i < inputs.length; i++) {      
            if(inputs[i].checked == true) { document.getElementById("checkterms").value = 'oki'; }    
            if(nr==1) {  goPlayer(); } 
            if(nr==2) {  goTrainer(); } 
        }   
    }


function  goPlayer(){ 
 
	url=tempdir+'/inside/form_player.php';
  //alert(url);
   document.getElementById("checkterms").value = 'oki';   
   if (window.ActiveXObject)
	{
		httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
	}
	else
	{
		httpRequest = new XMLHttpRequest();
	}
  var data = '' 
	httpRequest.open("POST", url, true);
	httpRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	httpRequest.send(data);
	httpRequest.onreadystatechange= function () {processRequest(); } ;
   
	return false;
}

function  goTrainer(){ 
	url=tempdir+'/inside/form_trainer.php';
  document.getElementById("checkterms").value = 'oki';   
 	if (window.ActiveXObject)
	{
		httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
	}
	else
	{
		httpRequest = new XMLHttpRequest();
	}
  var data = '' 
	httpRequest.open("POST", url, true);
	httpRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	httpRequest.send(data);
	httpRequest.onreadystatechange= function () {processRequest(); } ;
 
	return false;
}

function processRequest()
{
 
	if (httpRequest.readyState == 4)
	{
		if(httpRequest.status == 200)
		{
			var semka = document.getElementById("mytype");
      
			semka.innerHTML = httpRequest.responseText;
      checkForm.init();

   
      
		}
		else
		{
			alert("Chyba pri nacitani stanky"+ httpRequest.status +":"+ httpRequest.statusText);
		}
	}
}
    