 <div class="hbox mail"><div class='inmail'>
<h2>Newsletter</h2>


<style>

</style>
<div id="mlb2-4642631" class="ml-subscribe-form ml-subscribe-form-4642631">
    <div class="ml-vertical-align-center">
        <div class="subscribe-form ml-block-success" style="display:none">
            <div class="form-section">
                <p>Thank you! You have successfully subscribed to our newsletter.</p>
            </div>
        </div>
        <form class="ml-block-form" action="//app.mailerlite.com/webforms/submit/f2p4c0" data-id="362847" data-code="f2p4c0" method="POST" target="_blank">
            <div class="subscribe-form">
                <div class="form-section mb10">
                    <p>Přihlásit sa k odberu novinek</p>
                </div>
                <div class="form-section">
                    <div class="form-group ml-field-name ml-validate-required">
                        <input type="text" name="fields[name]" class="form-control" placeholder="Meno*" value="" autocomplete="name" x-autocompletetype="name" spellcheck="false" autocapitalize="off" autocorrect="off">
                    </div>
                    <div class="form-group ml-field-email ml-validate-required ml-validate-email">
                        <input type="email" name="fields[email]" class="form-control" placeholder="Email*" value="" autocomplete="email" x-autocompletetype="email" spellcheck="false" autocapitalize="off" autocorrect="off">
                    </div>
                </div>
                <input type="hidden" name="ml-submit" value="1" />
                <button type="submit" class="primary">
                    Prihlásit
                </button>
                <button disabled="disabled" style="display: none;" type="button" class="loading">
                    <img src="//static.mailerlite.com/images/rolling.gif" width="20" height="20" style="width: 20px; height: 20px;">
                </button>
            </div>
        </form>
        <script>
            function ml_webform_success_4642631() {
                var $ = ml_jQuery || jQuery;

                $('.ml-subscribe-form-4642631 .ml-block-success').show();
                $('.ml-subscribe-form-4642631 .ml-block-form').hide();
            };
        </script>
    </div>
</div>
<script type="text/javascript" src="//static.mailerlite.com/js/w/webforms.min.js?v98f07ba3d85ef7eb5404a058e826ec34"></script>


</div></div>