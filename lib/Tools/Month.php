<?php
namespace Tools;

class Month {

    protected static $month_names = array(
        1 => "leden",
        2 => "únor",
        3 => "březen",
        4 => "duben",
        5 => "květen",
        6 => "červen",
        7 => "červenec",
        8 => "srpen",
        9 => "září",
        10 => "říjen",
        11 => "listopad",
        12 => "prosinec",
    );

    static public function getFirstDay ($in_month, $in_year) {
        $date = $in_year."-".$in_month."-01";
        return date("Y-m-d", strtotime($date));
    }

    static public function getLastDay ($in_month, $in_year) {
        $first_day = strtotime(self::getFirstDay($in_month, $in_year));
        $date = $in_year."-".$in_month."-".date("t", $first_day);
        return date("Y-m-d", strtotime($date));
    }

    static public function getLastDayNum($in_month, $in_year) {
        $first_day = strtotime(self::getFirstDay($in_month, $in_year));
        $date = $in_year."-".$in_month."-".date("t", $first_day);
        return date("t", strtotime($date));
    }

    public static function getCalendar($in_month, $in_year) {
        $result = array();

        $first_day = strtotime(self::getFirstDay($in_month, $in_year)." 05:00:00");
        $last_day = strtotime(self::getLastDay($in_month, $in_year)." 05:00:00");
        for ($i = $first_day; $i <= $last_day; $i += 24*3600) {
            $result[date("Y-m-d", $i)] = date("w", $i);
        }

        return $result;
    }

    public static function getLongName($in_month) {
        $result = false;

        if ($in_month >= 1 && $in_month <= 12) {
            $result = self::$month_names[$in_month];
        }

        return $result;
    }
}
?>
