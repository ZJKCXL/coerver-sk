<?php

namespace Tools;

class ClassHelper
{

    private static function getBacktrace()
    {
        $trace = debug_backtrace();
        //print_r($trace);
        $caller = array_pop($trace);

        return $caller;
    }

    public static function getCallerClass()
    {
        $result = null;

        $caller = static::getBacktrace();

        $result = isset($caller['class']) ? $caller['class'] : $result;

        return $result;
    }

}
