<?php

if (!function_exists("import_request_variables")) {
    function import_request_variables($scope = false)
    {
        extract($_REQUEST, EXTR_REFS);
    }
}

if (!function_exists("eregi")) {
    function eregi($pattern, $string, &$regs = false)
    {
        return preg_match("/".$pattern."/", $string);
    }
}



if (!function_exists("mysql_query")) {
   //echo "k";
    function mysql_connect($server, $user_name, $password)
    {
        $result = false;

        $link = @mysqli_connect($server, $user_name, $password);
        if ($link) {
            $GLOBALS["link"] = $link;
            $result = $link;
        }

        return $result;
    }

    function mysql_select_db($in_db_name, $in_link = null)
    {
        if ($in_link == null) {
            $in_link = $GLOBALS["link"];
        }

        $result = mysqli_select_db($in_link, $in_db_name);

        return $result;
    }

    function mysql_query($in_query, $in_link = null)
    {
        if ($in_link == null) {
            $in_link = $GLOBALS["link"];
        }

        $result = mysqli_query($in_link, $in_query);
        return $result;
    }

    function mysql_num_rows($in_res)
    {
        return @mysqli_num_rows($in_res);
    }

    function mysql_fetch_array($in_res)
    {
        return @mysqli_fetch_array($in_res);
    }

    function mysql_fetch_assoc($in_res)
    {
        return @mysqli_fetch_assoc($in_res);
    }

    function mysql_fetch_row($in_res)
    {
        return @mysqli_fetch_row($in_res);
    }

    function mysql_affected_rows($in_link = null)
    {
        if ($in_link == null) {
            $in_link = $GLOBALS["link"];
        }
        return @mysqli_affected_rows($in_link);
    }

    function mysql_insert_id($in_link = null)
    {
        if ($in_link == null) {
            $in_link = $GLOBALS["link"];
        }
        return @mysqli_insert_id($in_link);
    }

    function mysql_error($in_link = null)
    {
        if ($in_link == null) {
            $in_link = $GLOBALS["link"];
        }
        return @mysqli_connect_error($in_link);
    }

    function mysql_data_seek($resource, $row_number)
    {
        return @mysqli_data_seek($resource, $row_number);
    }

    function mysql_real_escape_string($in_str, $in_link = null)
    {
        if ($in_link == null) {
            $in_link = $GLOBALS["link"];
        }
        return @mysqli_real_escape_string($in_link, $in_str);
    }

    function mysql_escape_string($in_str)
    {
        return @mysqli_escape_string($GLOBALS["link"], $in_str);
    }

    function mysql_close($in_link = null)
    {
        if ($in_link == null) {
            $in_link = $GLOBALS["link"];
        }
        return @mysqli_close($in_link);
    }

    function mysql_ping($in_link = null)
    {
        if ($in_link == null) {
            $in_link = $GLOBALS["link"];
        }
        return @mysqli_ping($in_link);
    }

    function mysql_free_result($res)
    {
        return @mysqli_free_result($res);
    }

    function mysql_result($res,$row=0,$col=0){ 
        $numrows = mysqli_num_rows($res); 
        if ($numrows && $row <= ($numrows-1) && $row >=0){
            mysqli_data_seek($res,$row);
            $resrow = (is_numeric($col)) ? mysqli_fetch_row($res) : mysqli_fetch_assoc($res);
            if (isset($resrow[$col])){
                return $resrow[$col];
            }
        }
        return false;
    }
    

}




?>
 