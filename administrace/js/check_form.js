/*
checkForm (version 3.4) by Riki "Fczbkk" Fridrich, 2003 - 2004
http://www.fczbkk.com/, mailto:riki@fczbkk.com
*/
//alert('test aktualnosti');
checkForm = {
	
	invalidMsg			: "Formulář není vyplněn správně.\nProsím opravte tato políčka:\n\n %err",
	errorMsg			: "\n- %err",
	errors				: [],

	invalidClass		: "invalid",
	requiredClass		: "required",
	outsideInvalidClass	: "outsideInvalid",

	submitOnceClass		: "submitOnce",
	alreadySubmitedClass: "alreadySubmited",
	
	fieldType			: [],
	defaultValue		: [],
	
	// inicializacia skriptu
	init : function () {
		// ak mame k dispozicii kniznice evt a cls a browser je standards-compliant, mozeme zacat
		if (evt && cls && document.getElementById) {
			// prejdeme vsetky formulare a najdeme v nich vsetky fieldy, zavesime potrebne eventy
			var forms = document.getElementsByTagName("form");
			for (var i = 0; i < forms.length; i++) {
				evt.add(forms[i], "submit", checkForm.checkForm);
				evt.add(forms[i], "reset", checkForm.checkForm);
				
				var fields = checkForm.getFields(forms[i]);
				for (var j = 0; j < fields.length; j++) {
					evt.add(fields[j], "blur", checkForm.checkField);
					evt.add(fields[j], "focus", checkForm.checkField);
					evt.add(fields[j], "change", checkForm.checkField);
					// kontrola pri onkeyup sposobuje problemy s nepristupnostou policka v niektorych prehliadacoch, ak obsahuje nespravnu hodnotu
					
					if ((fields[j].tagName.toLowerCase() == "input") && ((fields[j].type == "text") || (fields[j].type == "password"))) {
						evt.add(fields[j], "keyup", checkForm.checkField);
					}
					
				}
				
				checkForm.checkForm(forms[i]);
			}
			return true;
		}
		return false;
	},
	
	// vrati pole obsahujuce vsetky polia, ktore budeme kontrolovat
	getFields : function(frm) {
		if (frm && frm.getElementsByTagName) {
			var fields = [];
			
			var inputs = frm.getElementsByTagName("input");
			for (var i = 0; i < inputs.length; i++) {
				if (
					inputs[i].type == "text" ||
					inputs[i].type == "hidden" ||
					inputs[i].type == "password"
				) {
					fields[fields.length] = inputs[i];
				}
			}
			
			var textareas = frm.getElementsByTagName("textarea");
			for (var i = 0; i < textareas.length; i++) {
				fields[fields.length] = textareas[i];
			}
			
			var selects = frm.getElementsByTagName("select");
			for (var i = 0; i < selects.length; i++) {
				fields[fields.length] = selects[i];
			}
			return fields;
		}
		return false;
	},
	
	// preveri validitu vsetkych policok a povoli alebo nepovoli submit
	checkForm : function(frm) {
		if (!frm || !frm.tagName || frm.tagName.toLowerCase() != "form") {
			e = evt.fix(frm);
			frm = e.target;
		} else {
			var e = false;
		}
		
		checkForm.errors = [];
		
		var fields = checkForm.getFields(frm);
		var fieldsOK = true;
		for (var i = 0; i < fields.length; i++) {
				fieldsOK = ((checkForm.checkField(fields[i]) == "valid") && fieldsOK) ? true : false;
		}
		
		if (fieldsOK) {

			// kontrola formularov, ktore sa mozu odosielat iba raz
			if (e && (e.type == "submit") && cls.has(frm, checkForm.submitOnceClass)) {
				if (cls.has(frm, checkForm.alreadySubmitedClass)) {
					e.preventDefault;
					return false;
				} else {
					cls.add(frm, checkForm.alreadySubmitedClass);
				}
			}

			return true;
		} else {
			if (e && (e.type == "reset")) {
				return;
			}
			if (e && (e.type == "submit")) {
				var err = "";
				for (var i = 0; i < checkForm.errors.length; i++) {
					err += checkForm.errorMsg.replace("%err", checkForm.errors[i]);
				}
				alert(checkForm.invalidMsg.replace("%err", err));
				if (e.preventDefault) {
					e.preventDefault();
				}
			}
		}
		
		return false;
	},
	
	// preveri validitu policka a vrati "valid" alebo "invalid"
	checkField : function(elm) {
		if (!elm || !elm.tagName) {
			var e = evt.fix(elm);
			elm = e.target;
		}
		
		if (
			elm &&
			(
				(elm.tagName.toLowerCase() == "input") ||
				(elm.tagName.toLowerCase() == "select") ||
				(elm.tagName.toLowerCase() == "textarea")
			)
		) {
			var fieldOK = true;
			var elmClasses = cls.get(elm);
			for (var i = 0; i < elmClasses.length; i++) {
				if (checkForm.fieldType[elmClasses[i]]) {
					var rule = checkForm.fieldType[elmClasses[i]];
					if (fieldOK && typeof(rule) == "string") {
						
						// pravidlo je string
						if (elm.value != "") {
							fieldOK = (elm.value.search(new RegExp("^([" + rule + "]){1,}$")) < 0) ? false : true;
						}
						
					} else if (fieldOK && ((typeof(rule) == "function") || (typeof(rule) == "object")) && rule.source) {
						// Mozilla vracia RegExpu typ "function", zatial co ostatne browsery "object"
						
						// pravidlo je regExp
						if (elm.value != "") {
							fieldOK = (elm.value.search(rule) < 0) ? false : true;
						}
						
					} else if (fieldOK && (typeof(rule) == "function") && rule.prototype) {
						
						// pravidlo je funkcia
						fieldOK = rule(e, elm);
						
					}
				}

				/* default value
				if (e && checkForm.defaultValue[elmClasses[i]]) {
					var val = checkForm.defaultValue[elmClasses[i]];
					if ((e.type == "focus") && (elm.value == "")) {
						elm.value = val;
					}
					if ((e.type == "blur") && (elm.value == val)) {
						elm.value = "";
					}
				}
				*/
			}

			
			var outsideValidationOK = !cls.has(elm, checkForm.outsideInvalidClass);


			if (fieldOK && outsideValidationOK) {
				cls.remove(elm, "invalid");
			} else {
				cls.add(elm, "invalid");
				checkForm.errors[checkForm.errors.length] = checkForm.getFieldErrorMsg(elm);
				//(elm.checkFormErr) ? elm.checkFormErr : (elm.title) ? elm.title : (elm.name) ? elm.name : elm.toString();
			}
			
			return (fieldOK && outsideValidationOK) ? "valid" : "invalid";
		}
		return false;
	},
	
	// prida novy typ policka
	addFieldType : function(cls, rule, defaultValue) {
		if (cls && (rule || defaultValue)) {
			if (rule) {
				checkForm.fieldType[cls] = rule;
			}
			if (defaultValue) {
				checkForm.defaultValue[cls] = defaultValue;
			}
			return true;
		}
		return false;
	},
	
	getFieldErrorMsg : function(elm) {
		if (elm) {
			if (elm.checkFormErrorMsg) {
				return elm.checkFormErrorMsg;
			}
			if (elm.title) {
				return elm.title;
			}
			if (elm.id) {
				var labels = document.getElementsByTagName("label");
				for (var i = 0; i < labels.length; i++) {
					if (labels[i].attributes["for"] && (labels[i].attributes["for"].value == elm.id)) {
						// Opera nezvlada atribut "for"
						var str = checkForm.getNodeText(labels[i]);
						return str;
					}
				}
				return elm.id;
			}
			if (elm.name) {
				return elm.name;
			}
			return elm.toString();
		}
		return false;
	},
	
	getNodeText : function(node) {
		var str = "";
		if (node && node.hasChildNodes()) {
			for (var i = 0; i < node.childNodes.length; i++) {
				// TODO: skladanie toho stringu treba urobit nejak systemovejsie a prehladnejsie
				str += (node.childNodes[i].nodeType == 3) ? node.childNodes[i].nodeValue : checkForm.getNodeText(node.childNodes[i]);
				if ((node.childNodes[i].nodeType == 1) && (node.childNodes[i].tagName.toLowerCase() == "img") && (node.childNodes[i].attributes["alt"])) {
					str += node.childNodes[i].attributes["alt"].value;
				}
			}
		}
		return str;
	}
	
}

/* basic field types */

// required field
checkForm.addFieldType(
	"required",
	function(evt, elm) {
		if (elm) {
			if (elm.tagName.toLowerCase() == "select") {
				return (elm.value) ? true : false;
			} else {
				return (elm.value.search(/\S/) < 0) ? false : true;
			}
		}
		return false;
	}
);

// numbers only
checkForm.addFieldType(
	"numbers",
	"0123456789"
);

// IDs - numbers divided by space
checkForm.addFieldType(
	"ids",
	new RegExp("^[0-9 ]{1,}$")
);

// safe characters (alphanumeric, numbers and underscore)
checkForm.addFieldType(
	"safeChars",
	new RegExp("^[a-zA-Z0-9_]{1,}$")
);

//htlcode

checkForm.addFieldType(
	"htlCode",
	new RegExp("^[0-9][0-9]{4}$")
);

// date

// jednoducha kontrola pomocou regExp
/* 
checkForm.addFieldType(
	"date",						// in format (D)D.(M)M.YYYY
	new RegExp("^[0-9]{1,2}[.][0-9]{1,2}[.][0-9]{4}$")
);
*/

// komplexnejsia kontrola pomocou funkcie (pocty dni v mesiaci, prestupne roky a pod.)
checkForm.addFieldType(
	"date",
	function (evt, elm) {
		if (!elm.value) {return true;}

		var date = elm.value.split(".");
		var day = date[0];
		var month = date[1];
		var year = date[2];
		if (!isNaN(day) && !isNaN(month) && !isNaN(year)) {
			// mesiac
			if ((month > 12) || (month < 1)) {return false;}
			
			// dni
			if (day < 1) {return false;}
			
			var maxDays = 31;
			if (month == 2) {maxDays = (year%4 == 0) ? 29 : 28;} // kontrola prestupneho roku
			if ((month == 4) || (month == 6) || (month == 9) || (month == 11)) {maxDays = 30;}

			if (day > maxDays) {return false;}
			
			return true;
		}
		return false;
	}
);

// URL
checkForm.addFieldType(
	"url",
	new RegExp("^\\w+://(([\\w\\+\\.\\-]+\\b)(:\\w+)@)?([/\\w\\+\\.\\-]+\\b)(\/{1})?(\\?[\\w\\+\\.\\-/;\\&@=]+\\b)?(#[\\w\\-%]+\\b)?$"),
	//   protokol^      ^user       password^       ^server   konc. slash^       ^parametry (?)                 ^relativní odkaz (#)                                                
	//new RegExp("^[http|https|ftp]:\/\/[a-zA-Z0-9]+([-_\.]?[a-zA-Z0-9])*\.[a-zA-Z]{2,4}(\/{1}[-_~&=\?\.a-z0-9]*)*$"),
	"http://"
);

//domain
checkForm.addFieldType(
	"domain",
new RegExp("^[a-zA-Z0-9]+([-_\.]?[a-zA-Z0-9])*\.[a-zA-Z]{2,4}(\/{1}[-_~&=\?\.a-z0-9]*)*$")
);

// e-mail
checkForm.addFieldType(
	"email",
	new RegExp("^([\\w\\!\\#\\$\\%\\&\\*\\+\\-\\/\\=\\?\\^\\{\\}\\|\\~]+)((\\.){1}[\\w\\!\\#\\$\\%\\&\\*\\+\\-\\/\\=\\?\\^\\{\\}\\|\\~]+)*@[\\w\\!\\#\\$\\%\\&\\*\\+\\-\\/\\=\\?\\^\\{\\}\\|\\~]+((\\.){1}[\\w\\!\\#\\$\\%\\&\\*\\+\\-\\/\\=\\?\\^\\{\\}\\|\\~]+)+$")
	//new RegExp("^[a-z0-9]+[a-z0-9\._-]*[a-z0-9]+@[a-z0-9]+[a-z0-9\._-]*[a-z0-9]+\.[a-z]{2,4}$")
	//new RegExp("^[a-zA-Z0-9]+[a-zA-Z0-9\._-]*[a-zA-Z0-9]+@[a-zA-Z0-9]+[a-zA-Z0-9\._-]*[a-zA-Z0-9]+\.[a-zA-Z]{2,4}$")
);
checkForm.addFieldType(
        "string12",       
	      function(evt, elm) {
		                  if (elm) {
			                				return (elm.value.length > 12) ? false : true;
			                				
			                    		}
                       return false;
	                     }
);
checkForm.addFieldType(
        "string15",       
	      function(evt, elm) {
		                  if (elm) {
			                				return (elm.value.length > 16) ? false : true;
			                				
			                    		}
                       return false;
	                     }
);

checkForm.addFieldType(
        "string18",       
	      function(evt, elm) {
		                  if (elm) {
			                				return (elm.value.length > 19) ? false : true;
			                				
			                    		}
                       return false;
	                     }
);

checkForm.addFieldType(
        "string70",       
	      function(evt, elm) {
		                  if (elm) {
			                				return (elm.value.length > 70) ? false : true;
			                				
			                    		}
                       return false;
	                     }
);

checkForm.addFieldType(
        "string130",       
	      function(evt, elm) {
		                  if (elm) {
			                				return (elm.value.length > 130) ? false : true;
			                    		}
                       return false;
	                     }
);
checkForm.addFieldType(
        "string40",       
	      function(evt, elm) {
		                  if (elm) {
			                				return (elm.value.length > 40) ? false : true;
			                    		}
                       return false;
	                     }
);
checkForm.addFieldType(
        "string35",       
	      function(evt, elm) {
		                  if (elm) {
			                				return (elm.value.length > 35) ? false : true;
			                    		}
                       return false;
	                     }
);
checkForm.addFieldType(
        "string250",       
	      function(evt, elm) {
		                  if (elm) {
			                				return (elm.value.length > 250) ? false : true;
			                    		}
                       return false;
	                     }
);


checkForm.addFieldType(
        "string25",       
	      function(evt, elm) {
		                  if (elm) {
			                				return (elm.value.length > 25) ? false : true;
			                				
			                    		}
                       return false;
	                     }
);

checkForm.addFieldType(
        "string30",       
	      function(evt, elm) {
		                  if (elm) {
			                				return (elm.value.length > 30) ? false : true;
			                				
			                    		}
                       return false;
	                     }
);

checkForm.addFieldType(
        "string20",       
	      function(evt, elm) {
		                  if (elm) {
			                				return (elm.value.length > 20) ? false : true;
			                				
			                    		}
                       return false;
	                     }
);

checkForm.addFieldType(
        "string8",       
	      function(evt, elm) {
		                  if (elm) {
			                				return (elm.value.length > 8) ? false : true;
			                    		}
                       return false;
	                     }
);
checkForm.addFieldType(
        "extrastring6",       
	      function(evt, elm) {
		                  if (elm) {
			                				return ((elm.value.length != 6)&&(elm.value.length != 0)) ? false : true;
			                    		}
                       return false;
	                     }
);
checkForm.addFieldType(
        "extrastring8",       
	      function(evt, elm) {
		                  if (elm) {
			                				return ((elm.value.length != 8)&&(elm.value.length != 0)) ? false : true;
			                    		}
                       return false;
	                     }
);
checkForm.addFieldType(
         "extrastring68",
             function(evt, elm) {
                                 if (elm) {
                                                                       return (((elm.value.length != 8)&&(elm.value.length != 6))&&(elm.value.length != 0)) ? false : true;
                                                       }
                        return false;
                            }
);


checkForm.addFieldType(
	"maps",
new RegExp("^([-]{0,1})([0-9]{1,})([.]{0,1})(([0-9]{0,}))(,)([-]{0,1})([0-9]{1,})([.]{0,1})(([0-9]{0,}))$")
);


// zavolanie inicializacie checkForm-u pri zavedeni dokumentu
evt.add(window, "load", checkForm.init);


