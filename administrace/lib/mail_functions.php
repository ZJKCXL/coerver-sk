<?php

function czechize($in_str)
{
    //Predpoklada ze kodovani na serveru je ISO-8859-2
    //jinak $in_str = mb_convert_encoding($in_str, "ISO-8859-2")
       strtr($in_str, "ÚúŘřČčÉéÝýŽžŠšÁáĚěÍíÓóÚúÄËÖÜÂÎÔäëdöüâeîôuçďťňĎŤŇ" , "UuRrCcEeYyZzSsAaEeIiOoUuAEOUAIOaedouaeioucdtnDTN");
    return strtr($in_str, "\xe1\xe4\xe8\xef\xe9\xec\xed\xb5\xe5\xf2\xf3\xf6\xf5\xf4\xf8\xe0\xb9\xbb\xfa\xf9\xfc\xfb\xfd\xbe\xc1\xc4\xc8\xcf\xc9\xcc\xcd\xa5\xc5\xd2\xd3\xd6\xd5\xd4\xd8\xc0\xa9\xab\xda\xd9\xdc\xdb\xdd\xae",
    "aacdeeillnoooorrstuuuuyzAACDEEILLNOOOORRSTUUUUYZ");
    
     
     echo $in_str;
    
    
}
function czechizeIMG($in_str)
{
    //Predpoklada ze kodovani na serveru je ISO-8859-2
    // $in_str = mb_convert_encoding($in_str, "UTF-8")  ;
     strtr($in_str, "ÚúŘřČčÉéÝýŽžŠšÁáĚěÍíÓóÚúÄËÖÜÂÎÔäëdöüâeîôuçďťňĎŤŇ" , "UuRrCcEeYyZzSsAaEeIiOoUuAEOUAIOaedouaeioucdtnDTN");  
     return $in_str;
    
   
    
    
}
function normalize_filename($in_file_name)
{
     $in_file_name = str_replace("+","-",$in_file_name);
    $in_file_name = str_replace(" ","-",$in_file_name);
    $in_file_name = str_replace("*","-",$in_file_name);
    $in_file_name = str_replace("?","-",$in_file_name);
    $in_file_name = str_replace("/","-",$in_file_name);
    $in_file_name = str_replace("\\","-",$in_file_name);
    $in_file_name = str_replace("&","-",$in_file_name);
     $in_file_name = str_replace("---","-",$in_file_name);    
     $in_file_name = str_replace("--","-",$in_file_name);       
    $in_file_name = czechize($in_file_name);
    return $in_file_name;
}

function sendEmail($email_subject,$email_nadpis,$text,$news_nadpis,$news,$to_email,$from_email,$from_name,$mailid){

require_once("./classes/class.phpmailer.php");
         
$text = str_replace("../","http://www.rusnuclear.cz",$text);
// email start
$zprava .= "<table width=600><tr><td><img src=\"http://www.rusnuclear.cz/images/newsletter.jpg\" alt=\"Stáhnout obrázek\"></td></tr><tr><td><h2>";
$zprava .= $email_nadpis;
$zprava .= "</h2></td></tr><tr><td>";

if($text){
$zprava .= "<p>";
$zprava .= $text;
$zprava .= "</p>";
}

if(($news_nadpis)&&($news)){
$zprava .= "<h2>";
$zprava .= $news_nadpis;
$zprava .= "</h2>";
}

if($news){
$zprava .= $news;
}
$zprava .="</td></tr><tr><td align=center><a href=\"http://www.rusnuclear.cz/unbook/?email_id=".$mailid."\">Nemám již zájem o další zasílání e-mailového zpravodaje.</a></td></tr>";
$zprava .= "</table>";

$zpravatext = str_replace ("\n", "<br/>", $zprava);
$subject   = $email_subject;
$subject = czechize($subject);

$subject = strtr($subject, "ÚúŘřČčÉéÝýŽžŠšÁáĚěÍíÓóÚúÄËÖÜÂÎÔäëdöüâeîôuçďťňĎŤŇ" , "UuRrCcEeYyZzSsAaEeIiOoUuAEOUAIOaedouaeioucdtnDTN");
$plaintext = strtr($zprava, "ÚúŘřČčÉéÝýŽžŠšÁáĚěÍíÓóÚúÄËÖÜÂÎÔäëdöüâeîôuçďťňĎŤŇ" , "UuRrCcEeYyZzSsAaEeIiOoUuAEOUAIOaedouaeioucdtnDTN");
$plaintext = str_replace ("<br/>", "\n", $plaintext);
$plaintext = str_replace ("&nbsp;", " ", $plaintext);
$plaintext = str_replace ("<p>", "\n", $plaintext);
$plaintext = str_replace ("</p>", "\n", $plaintext);
$plaintext = str_replace ("</li>", "\n", $plaintext);
$plaintext = str_replace ("</ul>", "\n", $plaintext);
$plaintext = str_replace ("<td>", " ", $plaintext);
$plaintext = str_replace ("</td>", " ", $plaintext);
$plaintext = str_replace ("<table border=\"0\">", " ", $plaintext);
$plaintext = str_replace ("</table>", " ", $plaintext);
$plaintext = str_replace ("</tr>", "\n", $plaintext);
$plaintext = str_replace ("<tr>", " ", $plaintext);
$plaintext = str_replace ("</h2>", "\n\n", $plaintext);
$plaintext = str_replace ("<h2>", "", $plaintext);
$plaintext = str_replace ("</h3>", "\n", $plaintext);
$plaintext = str_replace ("<h3>", "\n", $plaintext);
$plaintext = strip_tags ($plaintext);
$text  = "";
$text .= file_get_contents("./classes/email_style_header.html");
$text .= file_get_contents("./css/emailing.css");
$text .= "</style></head><body>";
$text .= $zpravatext;
$text .= "</body></html>";
$text = stripslashes ( $text );
$text = str_replace ("ř", "&#345;", $text);
$text = str_replace ("Ř", "&#344;", $text);
$text = str_replace ("č", "&#269;", $text);
$text = str_replace ("Č", "&#268;", $text);
$text = str_replace ("é", "&#233;", $text);
$text = str_replace ("É", "&#278;", $text);
$text = str_replace ("š", "&#353;", $text);
$text = str_replace ("Š", "&#352;", $text);
$text = str_replace ("ň", "&#328;", $text);
$text = str_replace ("Ň", "&#327;", $text);
$text = str_replace ("ď", "&#271;", $text);
$text = str_replace ("Ď", "&#270;", $text);
$text = str_replace ("ť", "&#357;", $text);
$text = str_replace ("Ť", "&#356;", $text);
$text = str_replace ("ž", "&#382;", $text);
$text = str_replace ("Ž", "&#381;", $text);
$text = str_replace ("ú", "&#250;", $text);
$text = str_replace ("Ú", "&#218;", $text);
$text = str_replace ("ě", "&#283;", $text);
$text = str_replace ("Ě", "&#282;", $text);
$text = str_replace ("ů", "&#367;", $text);
$text = str_replace ("Ů", "&#366;", $text);
$htmltext = $text;


$mail = new PHPMailer();
$mail->IsSMTP();   // set mailer to use SMTP
$mail->Host = "81.0.231.161"; 
$mail->SMTPAuth = false;     // turn on SMTP authentication
$mail->Username = " ";  // SMTP username
$mail->Password = " "; // SMTP password
$mail->From = $from_email;
$mail->FromName = $from_name;
$mail->AddReplyTo("$from_email");
$mail->AddAddress("$to_email");
//$mail->AddBCC("$to_copy_email");
$mail->WordWrap = 50;
//$mail->AddAttachment("http://www.rusnuclear.cz/images/newsletter.jpg", "newsletter.jpg");    // optional name
$mail->IsHTML(true);
$mail->Subject = "$subject";
$mail->Body    = "$htmltext";
$mail->AltBody = "$plaintext";


if(!$mail->Send())
{
 return false;
}

}
?>
