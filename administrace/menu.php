<?php
function makeMenu($odkaz){
  $active = " .".substr($_SERVER['REQUEST_URI'],strpos($_SERVER['REQUEST_URI'],'index.php')+13);
  if(($active == $odkaz)||($active."new" == $odkaz)){ $ifactiv = ' active" '; } else { $ifactiv = '" '; }
  echo $ifactiv."href='index.php?id=".$odkaz."' onmouseover='openMine(\"".$odkaz."\")'";
}
function makeSubMenu($odkaz,$sef){
  $active = substr($_SERVER['REQUEST_URI'],strpos($_SERVER['REQUEST_URI'],'index.php')) ;
  if($active == $odkaz){ $ifactiv = ' active" '; } else { $ifactiv = '" '; }
  echo $ifactiv."href='index.php?id=".$odkaz."' onmouseOver='openMine(\"".$sef."\")'";
}
?>
<script language="javascript" type="text/javascript">
  var timerID;
  function wait(nr){
  clearTimeout(timerID);
  }
  function openMine(odkaz){
  var thismenu = document.getElementById('thismenu');
  var uls = thismenu.getElementsByTagName('UL');
  for (var i=0;i<(uls.length);i++)
	{
	if(uls[i].id != odkaz) { closeMineQuick(uls[i].id);}
	}
  if(document.getElementById(odkaz)){
  document.getElementById(odkaz).style.display = 'block';
  clearTimeout(timerID);
  }
  }
  function closeMine(odkaz){
  if(document.getElementById(odkaz)){
  timerID = setTimeout("document.getElementById('"+odkaz+"').style.display = 'none'",1000);
  }
  }
  function closeMineQuick(odkaz){
  if(document.getElementById(odkaz)){
  document.getElementById(odkaz).style.display = 'none';
  }
  }
</script>
<ul id="thismenu">

<?php

 
	if(@has_priviledge(PRIVILEDGE_CONTENT))
	{
?>
<li class="topmenu"><a class="content<?php echo makeMenu('pages'); ?>>Obsahové stránky</a>
<?php
  	if(@has_priviledge(PRIVILEDGE_SECTIONS)){
?>
  <ul id="pages" onmouseOut='closeMine("pages")'>
  <li><a class="groups<?php echo makeSubMenu('menugroups','pages'); ?>>Sekce Webu</a></li>
  </ul>
<?php
  	}

?>

</li>
<?php
  	}
 

	if(@has_priviledge(PRIVILEDGE_CONTENTEN))
	{ 
?>
<li class="topmenu"><a class="content<?php echo makeMenu('pages&type=eng'); ?>>Obsahové stránky EN</a>
  <ul id="pages&type=eng" onmouseOut='closeMine("enpages")'>
  <li><a class="groups<?php echo makeSubMenu('enmenugroups','pages&type=eng'); ?>>Sekce Webu EN</a></li>
  </ul>
</li>

<?php
  }
	if(@has_priviledge(PRIVILEDGE_ANSWRZNONE))
	{
?>
<li class="topmenu"><a class="question<?php echo makeMenu('dotazy'); ?>>Dotazy</a>
<?php
	if(@has_priviledge(PRIVILEDGE_SETTING))
	{
?>
  <ul id="dotazy" onmouseOut='closeMine("dotazy")'>
  <li><a class="servis<?php echo makeSubMenu('stats','dotazy'); ?>>Statistiky</a></li>
  <li><a class="servis<?php echo makeSubMenu('katpor&catname=dotaz','dotazy'); ?>>Typ dotazu</a></li>
  <!--<li><a class="servis<?php echo makeSubMenu('katpor&catname=drogy','dotazy'); ?>>Typy drog</a></li>-->
  <li><a class="servis<?php echo makeSubMenu('katpor&catname=kdo','dotazy'); ?>>Typ zadavatele</a></li>
  <li><a class="servis<?php echo makeSubMenu('katpor&catname=status','dotazy'); ?>>Stavy dotazů</a></li>
  <!--<li><a class="servis<?php echo makeSubMenu('katpor&catname=money','dotazy'); ?>>Ceny</a></li>-->
  </ul>
<?php
  }
  ?>
  </li>
  <?php
  }
?>
 
 

<?php if(@has_priviledge(PRIVILEDGE_AKTIVITIES)) 	{ ?>
<li class="topmenu"><a class="rubriky<?php echo makeMenu('aktivity'); ?>>Užitečné odkazy</a></li>
<?php } ?>

<?php
 	if(@has_priviledge(PRIVILEDGE_HMBOX))
	{                                       
?>

<li class="topmenu"><a class="content" href="index.php?id=pages&type=hmboxes">Widgety</a></li>
<?php if(@has_priviledge(PRIVILEDGE_MICRODATA)) 	{ ?>
 <li class="topmenu"><a class="content" href="index.php?id=contacts">Microdata</a></li> 
 <?php } ?>
<?php                                                                                    
}
?>

<?php 
	if(@has_priviledge(PRIVILEDGE_ANNUAL)){
?>                    
       
<li class="topmenu"><a class="content<?php echo makeMenu('stanovyusers'); ?>>Stanovy</a></li>
<li class="topmenu"><a class="content<?php echo makeMenu('konfusers'); ?>>Výroční s.</a>
  <ul id="konfusers" onmouseOut='closeMine("pages&type=schuze")'>
  <li><a class="groups<?php echo makeSubMenu('pages&type=schuze','konfusers'); ?>>Text</a></li>
    <li><a class="groups<?php echo makeSubMenu('konfusers2015','konfusers'); ?>>Hlasování 2015</a></li>    
    <li><a class="groups<?php echo makeSubMenu('konfusers2014','konfusers'); ?>>Hlasování 2014</a></li>  
    <li><a class="groups<?php echo makeSubMenu('konfusers2013','konfusers'); ?>>Hlasování 2013</a></li>
  <li><a class="groups<?php echo makeSubMenu('konfusers2012','konfusers'); ?>>Hlasování 2012</a></li>
  <li><a class="groups<?php echo makeSubMenu('konfusers2011','konfusers'); ?>>Hlasování 2011</a></li>
  </ul>
</li>
<?php }
 ?>
<?php 
	if(@has_priviledge(PRIVILEDGE_ANNUAL)){
?>
<li class="topmenu"><a class="content<?php echo makeMenu('poolusers'); ?>>Online hlasování.</a>
  <ul id="poolusers" onmouseOut='closeMine("pages&type=pool")'>
  <li><a class="groups<?php echo makeSubMenu('pages&type=pool','poolusers'); ?>>Text</a></li>
 
  </ul>
</li>
<?php }
 ?>

<?php if($galeries_ok == 1){
	if(@has_priviledge(PRIVILEDGE_PHOTO)){
?>
<li class="topmenu"><a class="foto<?php echo makeMenu('galery'); ?>>Fotogalerie</a>
<?php if($katgal_ok > 0){ ?>
  <ul id="galery" onmouseOut='closeMine("galery")'>
  <li><a class="foto<?php echo makeSubMenu('kat&catname=galcats','galery'); ?>>Kategorie</a></li>
  </ul>
  <?php } ?>
</li>
<?php }
}

if($news_ok == 1){
	if(@has_priviledge(PRIVILEDGE_AKTUAL)){
?>
<li class="topmenu"><a class="time<?php echo makeMenu('pages&type=aktualne'); ?>>Aktuálně</a>
<?php if($aktucat_ok == 1){ ?>
  <ul id="pages&type=aktualne" onmouseOut='closeMine("aktualne")'>
  <li><a class="time<?php echo makeSubMenu('kat&catname=aktucats','pages&type=aktualne'); ?>>Kategorie</a></li>
  </ul>
  <?php } ?>
</li>
<?php }
}

 
	if(@has_priviledge(PRIVILEDGE_ARTICLES)){
?>
<li class="topmenu"><a class="time<?php echo makeMenu('pages&type=articles'); ?>>Blog</a>
<?php if($aktucat_ok == 1){ ?>
  <ul id="pages&type=aktualne" onmouseOut='closeMine("aktualne")'>
  <li><a class="time<?php echo makeSubMenu('kat&catname=artcats','pages&type=articles'); ?>>Kategorie</a></li>
  </ul>
  <?php } ?>
</li>
<?php }



 
	if(has_priviledge(PRIVILEDGE_REGZ)){
?>

<li class="topmenu"><a class="admins<?php echo makeMenu('konfregz'); ?>>Registrace</a>
  <!--<ul id="konfregz" onmouseOut='closeMine("konfregz")'>
   <li><a class="servis<?php echo makeSubMenu('konfregz&catname=x','konfregz'); ?>>nic</a></li>  <li><a class="servis<?php echo makeSubMenu('konfregz&catname=x','konfregz'); ?>>nic</a></li> 
  </ul> --> 
</li>
<?php }
 
 ?>

<?php
 	if(has_priviledge(PRIVILEDGE_DATA))
	{
?>
 

<li class="topmenu"><a class="content<?php echo makeMenu('regz'); ?>">DB Beta</a>
  
</li>



<?php
  }

 	if(has_priviledge(PRIVILEDGE_COURSE))
	{
?>
 

<li class="topmenu"><a class="content<?php echo makeMenu('excels'); ?>">Data</a>
  <ul id="excels" onmouseOut='closeMine("excels")'>
  <li><a class="rubriky<?php echo makeSubMenu('regz','excels'); ?>>DB Beta</a></li>
 
  </ul>
</li>


<li class="topmenu"><a class="admins<?php echo makeMenu('turnament'); ?>">Cup</a>
</li>

 

 


<li class="topmenu"><a class="admins<?php echo makeMenu('kempy'); ?>">Kempy & Akademie</a>
  <ul id="kempy" onmouseOut='closeMine("kempy")'>
  <li><a class="rubriky<?php echo makeSubMenu('kat&catname=kempy','kempy'); ?>>Kategorie</a></li>
  <li><a class="rubriky<?php echo makeSubMenu('kat&catname=partners','kempy'); ?>>Partneři</a></li>  
  </ul>
</li>
<!--
<li class="topmenu"><a class="admins<?php echo makeMenu('kempysk'); ?>">Kempy & Akademie SK</a>
  <ul id="kempysk" onmouseOut='closeMine("kempy")'>
  <li><a class="rubriky<?php echo makeSubMenu('kat&catname=kempysk','kempysk'); ?>>Kategorie SK</a></li>
  </ul>
</li>
-->
<li class="topmenu"><a class="admins<?php echo makeMenu('course'); ?>">Kurzy</a>
  <ul id="course" onmouseOut='closeMine("course")'>
  <li><a class="rubriky<?php echo makeSubMenu('kat&catname=course','course'); ?>>Kategorie</a></li>
  </ul>
</li>
</li>
<!--
<li class="topmenu"><a class="admins<?php echo makeMenu('coursesk'); ?>">Kurzy SK</a>
  <ul id="coursesk" onmouseOut='closeMine("coursesk")'>
  <li><a class="rubriky<?php echo makeSubMenu('kat&catname=coursesk','coursesk'); ?>>Kategorie SK</a></li>
  </ul>
</li> -->
<?php
}
?>

 <?php
 	if(@has_priviledge(PRIVILEDGE_MEMBERS))
	{
?>

<li class="topmenu"><a class="content" href="index.php?id=members">Trenéři</a></li>
<?php
}
?>


<?php 
if(has_priviledge(PRIVILEDGE_ADDS)){ ?>
<li class="topmenu"><a class="rubriky<?php echo makeMenu('banners'); ?>>Bannery</a></li>
<!--<li class="topmenu"><a class="short<?php echo makeMenu('enbanners'); ?>>[EN]</a></li>-->
<?php } ?>


<?php if($katdowns_ok == 1){
	if(@has_priviledge(PRIVILEDGE_DOWNLOADS))
	{
?>
<li class="topmenu"><a class="downloads<?php echo makeMenu('downloads'); ?>>Downloads</a>
  <ul id="downloads" onmouseOut='closeMine("downloads")'>
  <li><a class="rubriky<?php echo makeSubMenu('kat&catname=downloads','downloads'); ?>>Kategorie</a></li>
  </ul>
</li>
<?php }
}
 ?>

 <?php
 	if(@has_priviledge(PRIVILEDGE_STAGE))
	{                                       
?>  
<li class="topmenu"><a class="admins<?php echo makeMenu('dotaznik&stage=1'); ?>">Dotazníky</a></LI>
<?php
}
  
?>
 
<?php
 	if(@has_priviledge(PRIVILEDGE_CFD))
	{                                       
?>  
<li class="topmenu"><a class="admins<?php echo makeMenu('cfdOnly2016'); ?>">CFD 2016</a>
<li class="topmenu"><a class="admins<?php echo makeMenu('dotaznik'); ?>">Dotazníky</a>
 <ul id="dotaznik" onmouseOut='closeMine("dotaznik")'>
  <li><a class="rubriky<?php echo makeSubMenu('dotaznikold','dotaznik'); ?>>2015</a></li>
  </ul>
</li>                                                                                               
<li class="topmenu"><a class="admins<?php echo makeMenu('cfd2016'); ?>">Registrovaní</a>
 <ul id="cfd2016" onmouseOut='closeMine("cfd2016")'>
  <li><a class="rubriky<?php echo makeSubMenu('emailTxt','cfd2016'); ?>>Text Emailu</a></li>
  </ul>
</li>
<?php
}
?>

<?php

	if(@has_priviledge(PRIVILEDGE_BANK))
	{
?>

<li class="topmenu"><a class="calender" href="index.php?id=bank">Banka</a></li>
<?php
   }

	if(@has_priviledge(PRIVILEDGE_SECURE))
	{
?> 
<li class="topmenu"><a class="admins<?php echo makeMenu('secure'); ?>">Zabez. sekce</a> 
</li>

<?php
}
	if(@has_priviledge(PRIVILEGE_OBJECTS))
	{
?>
<li class="topmenu"><a class="calender" href="index.php?id=objects">Objekty</a></li>

<?php
}
	if(@has_priviledge(PRIVILEDGE_USERS))
	{
?>
<li class="topmenu"><a class="admins" href="index.php?id=users">Users</a></li>
<?php
}


?>
</ul>
