 <table id='tabplayer'  style='margin-bottom: 10px; padding-left:0px;'>
        <tr>
        <td  colspan=5>   
        <br/>
        <h2>HRÁČ </h2>                                                
        </td>
        </tr>  
        
         <tr>
 
        <td><input type="text" placeholder='Jméno hráče'required="required" name="playername"  value="<?php echo strip_tags($playername)?>" />
        </td><td>&nbsp;</td>
        <td><input type="text" placeholder='Příjmení hráče'required="required"  name="playersurname" value="<?php echo strip_tags($playersurname)?>"   />
        </td>
        </tr>
        
        <tr>
        <td><input type="text" placeholder='Klub' required="required"  name="club"  value="<?php echo strip_tags($club)?>" id="frmClub"  />
        </td><td>&nbsp;</td> 
        <?php
                   // $value = strip_tags($birth);
                  //  $kousky = explode ('.', $value);
                  // $birth = $kousky[2]."-".$kousky[1]."-".$kousky[0];
        ?>
        <td><input type="text"   placeholder='DD.MM.RRRR' required="required"  title="Pole Datum narození musí obsahovat platné datum ve formátu DD.MM.RRRR" name="birth"  value="<?php echo strip_tags($birth)?>" id="frmBirth"  />
        </td>
        </tr>      

   

         </table>
         <?php 
  
            if($iamoknew == 1) {  $checked = ' checked '; } else {  $checked = '';  }
        ?>
              <p style='font-size: 12px; padding-left: 0px;'>
               <input <?php echo $checked; ?> type="checkbox" required="" class="required short " title="• Musíte zaškrtnout pole Souhlasím..." name="iamoknew" value="1" id="Musíte zaškrtnout pole Souhlasím...">  <strong>Souhlas se zpracováním osobních údajů</strong> Tímto uděluje Zákonný zástupce společnosti Společnost Moderní fotbal&nbsp;s.r.o., se sídlem Drtinova 10/557, 150 00 Praha 5, IČ: 24851701, zapsaná v obchodním rejstříku vedeném Městským soudem v Praze pod spisovou značkou 201581 C, jako správce osobních údajů (dále jen &bdquo;Organizátor"), <strong>svobodný a dobrovolný souhlas se zpracováním osobních údajů zákonného zástupce a účastníka akcí Coerver<sup>&reg;</sup> Coaching v rozsahu:</strong>&nbsp;fakturační údaje, jméno a příjmení, datum narození, příslušnost ke sportovnímu oddílu, rodné číslo, jméno a/nebo číslo zdravotní pojišťovny, e-mail, telefon, velikost oblečení, jmenovka na dres, obrazové i zvukové záznamy osoby a podobizny účastníka akcí <strong>za účelem:&nbsp;</strong>realizace zakoupených služeb a produktů a oslovování s nabídkou dalších produktů a služeb společnosti Moderní fotbal, včetně informování o produktech, službách, akcích, soutěžích, odebírání novinek a zasílání přání k&nbsp;svátkům a narozeninám, pořizování foto a video dokumentace z&nbsp;realizace služeb a vedení účetnictví.</p><br/>
               
              <p style='font-size: 12px; padding-left: 0px;'>Zákonný zástupce účastníka akcí Coerver&reg; Coaching dále dává Organizátorovi bezúplatný souhlas k pořízení a použití a ke zpracování obrazových i zvukových záznamů jeho osoby, podobizny, a jiných projevů osobní povahy pořízených v souvislosti s konáním akcí v jakékoliv formě a bez omezení pro účely marketingu, reklamy a propagace Organizátora, jeho činnosti a akcí pořádaných Organizátorem nebo v jeho kooperaci, jakožto i pro účely marketingu, reklamy a propagace jeho obchodních partnerů, a to bez nároku na jakoukoliv odměnu.</p>
               
              <p style='font-size: 12px; padding-left: 0px;'><strong>Souhlas je udělen na dobu 5 let.</strong><strong> </strong></p><br/>
            
              <p style='font-size: 12px; padding-left:  0px;'>Dále zákonný zástupce účastníka akcí Coerver® Coaching bere na vědomí, že tento souhlas může kdykoliv odvolat elektronicky na adrese <a href="mailto:info@coerver.cz">info@coerver.cz</a> nebo písemně na adrese společnosti Moderní fotbal&nbsp;s.r.o. Souhlas mohu odvolat i před uplynutím doby, na kterou byl udělen. Odvoláním souhlasu není dotčena zákonnost zpracování osobních údajů před odvoláním souhlasu. Obecné informace o přístupu k ochraně osobních údajů jsou k dispozici také na webové adrese&nbsp; &nbsp;<a href="/osobni-udaje.html" target="_blank">https://www.coerver.cz/osobni-udaje</a>.</p>