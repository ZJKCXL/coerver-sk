<?php
function repairDate($date_in ) {
      if(strpos($date_in,'.') > 0)  { 
            $kousky = explode ('.', $date_in);
            $newdate =  $kousky[2]."-".$kousky[1]. "-".$kousky[0]; 
        }
      return $newdate;
    }
?>
 
 <table id='tabplayer'>
        <tr>
        <td  colspan=5>   
        <br/>
        <h2>HRÁČ </h2>                                                
        </td>
        </tr>  
        
         <tr>

        <td><input type="text" placeholder='Meno hráča'required="required" name="playername"  value="<?php echo strip_tags($playername); ?>" />
        </td><td>&nbsp;</td>
        <td  ><input type="text" placeholder='Priezvisko hráča'required="required"  name="playersurname" value="<?php echo strip_tags($playersurname); ?>" />
        </td>
        </tr>
        
        <tr>
        <td><input type="text" placeholder='Klub' required="required"  name="club"  value="<?php echo strip_tags($club); ?>" id="frmClub"  />
        </td><td>&nbsp;</td> 
        <td><input type="text"  placeholder='Dátum narodenia DD.MM.RRRR' required="required"  title="&bull; Pole Datum narození musí obsahovat platné datum ve formátu DD.MM.RRRR" name="birth"  value="<?php echo repairDate(strip_tags($birth)); ?>" id="frmBirth"  />
        </td>
        </tr>      
</table>
<p>&nbsp;</p>
<?php 
  
            if($iamoknew == 1) {  $checked = ' checked '; } else {  $checked = '';  }
        ?>
              <p style='font-size: 12px; padding-left: 0px;'>
               <input <?php echo $checked; ?> type="checkbox" required="" class="required short " title="• Musíte zaškrtnout pole Souhlasím..." name="iamoknew" value="1" id="Musíte zaškrtnout pole Souhlasím...">  <strong>Súhlas so spracovaním osobných údajov</strong> Týmto udeľuje Zákonný zástupca spoločnosti   Moderní fotbal s.r.o., so sídlom Drtinova 10/557, 150 00 Praha 5, IČO: 24851701, zapísaná v obchodnom registri vedenom Mestským súdom v Prahe pod spisovou značkou 201581 C, ako správca osobných údajov (ďalej len „spoločnosť Moderní fotbal”),  <strong>slobodný a dobrovoľný súhlas so spracovaním osobných údajov zákonného zástupcu a účastníka akcií Coerver® Coaching v rozsahu: </strong>&nbsp;fakturačné údaje, meno a priezvisko, dátum narodenia, príslušnosť ku športovému oddielu, rodné číslo, meno alebo číslo zdravotnej poisťovne, e-mail, telefón, veľkosť oblečenia, menovka na drese, obrazové aj zvukové záznamy osoby a podobizne účastníka akcií  <strong>za účelom:&nbsp;</strong> realizácie zakúpených služieb a produktov a oslovovanie s ponukou ďalších produktov a služieb spoločnosti Moderní fotbal, v rámci informovaní o produktoch, službách, akciách, súťažiach, odoberaniu noviniek a zasielaniu priania k meninám a narodeninám, ku robeniu fotografií a video dokumentácií z realizácie služieb a vedeniu účtovníctva.</p><br/>
 
               
              <p style='font-size: 12px; padding-left: 0px;'><strong>Súhlas je udelený na dobu 5 rokov.</strong><strong> </strong></p><br/>
            
              <p style='font-size: 12px; padding-left:  0px;'>Zákonný zástupca <strong>účastníka akcií Coerver® Coaching</strong> berie na vedomie, že tento súhlas môže kedykoľvek odvolať elektronicky na adrese  <a href="mailto:info@coerver.sk">info@coerver.sk</a> sk , alebo písomne na adrese spoločnosti Moderní fotbal s.r.o. Súhlas môže odvolať i pred uplynutím doby, na ktorú bol udelený. Odvolaním súhlasu nie je dotknutá zákonnosť spracovania osobných údajov pred odvolaním súhlasu. Všeobecné informácie  o prístupe k ochrane osobných údajov sú k dispozícii na webovej adrese &nbsp;<a href="/osobne-udaje.html" target="_blank">https://www.coerver.cz/osobne-udaje</a>.</p>