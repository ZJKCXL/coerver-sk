<table id='tabtrain'  style='margin-bottom: 10px; padding-left:  0px;'>
  
              <tr>
        <td colspan=5 > <br/>  
        <h2>TRENÉR</h2> 
        </td>
        </tr> 
 <tr> 
         <td><input type="text" placeholder='Jméno trenéra' required="required"  name="trainame"  value="<?php echo strip_tags($trainame); ?>" id="frmtName"  />
        </td><td>&nbsp;</td> 
        <td  ><input type="text" placeholder='Příjmení trenéra' required="required"  name="trainsurname" value="<?php echo strip_tags($trainsurname); ?>" id="frmxtSurname"  />
        </td>
        </tr>

       <tr>
        
        <td><input type="text" placeholder='Klub' required="required"    name="club"  value="<?php echo strip_tags($club); ?>" id="frmClub"  />
        </td> <td>&nbsp;</td>
         
         <td><input type="text" placeholder='Předchozí klub'   name="formerclub"  value="<?php echo strip_tags($formerclub); ?>" id="frmFClub"  />
        </td> 
        
        </tr>   

        
        <tr>
        <?php
                //    $value = strip_tags($birth);
                 //   $kousky = explode ('.', $value);
                //    $birth = $kousky[2]."-".$kousky[1]."-".$kousky[0];
        ?>
        <td><input type="text"     placeholder='Datum narození DD.MM.RRRR' required="required"  name="birth"  value="<?php echo strip_tags($birth); ?>" id="frmtBirth"  />
        </td><td>&nbsp;</td>


        <td  >
       <input type="text"  placeholder='Trenérské vzdělání' required="required"   name="maxschool"  value="<?php echo strip_tags($maxschool); ?>" id="frmtBirth"  /> 
        </td>
        </tr>
 
        
        </table>

        <?php 
            if($iamoknew == 1) {  $checked = ' checked '; } else {  $checked = '';  }
        ?>
          
        
        <p style='font-size: 12px; padding-left: 0px;'>
        <input <?php echo $checked; ?> type="checkbox" required="" class="required short " title="• Musíte zaškrtnout pole Souhlasím..." name="iamoknew" value="1" id="Musíte zaškrtnout pole Souhlasím..."> <strong>Souhlas se zpracováním osobních údajů</strong>  Tímto uděluji společnosti Společnost Moderní fotbal&nbsp;s.r.o., se sídlem Drtinova 10/557, 150 00 Praha 5, IČ: 24851701, zapsaná v obchodním rejstříku vedeném Městským soudem v Praze pod spisovou značkou 201581 C, jako správce osobních údajů (dále jen &bdquo;společnost Moderní fotbal"), <strong>svobodný a dobrovolný souhlas se zpracováním osobních údajů v rozsahu:</strong>&nbsp;fakturační údaje, jméno a příjmení, datum narození, příslušnost ke sportovnímu oddílu, dosažené trenérské vzdělání, e-mail, telefonní číslo, obrazové i zvukové záznamy mé osoby a podobizny <strong>za účelem:</strong>&nbsp;realizace zakoupených služeb a produktů a oslovování s nabídkou dalších produktů a služeb společnosti Moderní fotbal, včetně informování o produktech, službách, akcích, soutěžích, odebírání novinek a zasílání přání k&nbsp;svátkům a narozeninám, pořizování foto a video dokumentace z&nbsp;realizace služeb a vedení účetnictví.</p>
        <p>&nbsp;</p>
        <p style='font-size: 12px; padding-left: 0px;'><strong>Souhlas je udělen na dobu 5 let.</strong><br /> <br /> Dále beru na vědomí, že tento souhlas mohu kdykoliv odvolat elektronicky na adrese  <a href="mailto:info@coerver.cz">info@coerver.cz</a> nebo písemně na adrese společnosti Moderní fotbal&nbsp;s.r.o. Souhlas mohu odvolat i před uplynutím doby, na kterou byl udělen. Odvoláním souhlasu není dotčena zákonnost zpracování osobních údajů před odvoláním souhlasu. Obecné informace o přístupu k ochraně osobních údajů jsou k dispozici také na webové adrese <a href="/osobni-udaje.html" target="_blank">https://www.coerver.cz/osobni-udaje</a>.</p></div>