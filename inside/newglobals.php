<?php
class Globals {
  public static $META_DESC_LENGTH_IDEAL = 160; 
  public static $META_KEYWORDS_LENGTH_IDEAL = 160; 
  public static $META_TITLE_LENGTH_IDEAL = 60; 
  public static $META_DESC_LENGTH = 200; 
  public static $META_KEYWORDS_LENGTH = 200; 
  public static $META_TITLE_LENGTH = 70;  
  public static $GLOBAL_DASHBOARD = '1AR23_XiOT6wDOsHc_wnIgTAqbqo-fiNt';
  public static $GLOBAL_FILEMANAGER = 1;   //0 neni, 1 je
  public static $GLOBAL_HOME = "/var/www/virtual/coerver.cz/htdocs/www";   //puvodni globalgal 
  public static $GLOBAL_BASE_URL = 'https://www.coerver.cz/';     
  public static $GLOBAL_WEB_IMGS_PATH = '/var/www/virtual/coerver.cz/htdocs/www/web_images/';   
  public static $GLOBAL_DOWN =  "/var/www/virtual/coerver.cz/htdocs/www/downloads/";
  public static $GLOBAL_DOWN_PREV =  "/var/www/virtual/coerver.cz/htdocs/www/downloads/preview_images/";
  public static $GLOBAL_DOWN_PREV_SMALL =   "/var/www/virtual/coerver.cz/htdocs/www/downloads/preview_images/thumbs/";
  public static $GLOBAL_GAL_IMGS_URL = '/finalgalpics/';   
  public static $GLOBAL_WEB_IMGS_URL = '/web_images/';       
  public static $GLOBAL_MODULES_BLOG = 'Blog';   
  public static $GLOBAL_MODULES_MEMBERS = 'Trenéři';     
  public static $GLOBAL_ADMIN_PAGER = 15; 
  public static $GLOBAL_GAL_REVERSE = 15; 
  public static $GLOBAL_SQL_FILE = '/sql/db.php'; 
  public static $GLOBAL_SQL_NEWFILE = '/sql2018/db.php'; 
  public static $GLOBAL_SQL_FILE_WIN = '/sql2018/dbEX.php'; 
  public static $GLOBAL_HEADERS_STICKY = 1;   
  public static $GLOBAL_HEADERS_FONTAWESOME = 1;     
  public static $GLOBAL_HEADERS_JQUERY = 1;   
  public static $GLOBAL_HEADERS_SLICK = 1; 
  public static $GLOBAL_HEADERS_SWIPEBOX = 1; 
  public static $GLOBAL_HEADERS_RESPCSS = 1;  
  public static $GLOBAL_HEADERS_TINYCSS = 1;  
  public static $GLOBAL_WEB_NAME = "Coerver Coaching ";
  public static $GLOBAL_SHOW_ERRORZ = 1;
  public static $GLOBAL_TEMP_DIR = '';    
  public static $GLOBAL_ADMINPAGES = 'adminpages';
  public static $GLOBAL_ADMINPAGES_FULLPATH = '/var/www/virtual/coerver.cz/htdocs/www/adminpages/';
  public static $GLOBAL_VISTOS_AVIABLE = 0;
  public static $GLOBAL_MAILZGO_TRAP = 0;

  // pouziti pak Globals::$GLOBAL_VISTOS_AVIABLE 
 }
// echo Globals::$GLOBAL_ADMINPAGES;
?>
