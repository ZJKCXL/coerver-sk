<table id='tabtrain'>
        <tr>
                <td colspan=5> <br/>
                        <h2>TRÉNER</h2>
                </td>
        </tr>
        <tr>
                <td><input type="text" placeholder='Meno trénera' required="required" name="trainame" value="<?php echo strip_tags($trainame); ?>"
                                id="frmtName" />
                </td>
                <td>&nbsp;</td>
                <td><input type="text" placeholder='Priezvisko trénera' required="required" name="trainsurname" value="<?php echo strip_tags($trainsurname); ?>"
                                id="frmxtSurname" />
                </td>
        </tr>

        <tr>

                <td><input type="text" placeholder='Klub' required="required" name="club" value="<?php echo strip_tags($club); ?>"
                                id="frmClub" />
                </td>
                <td>&nbsp;</td>

                <td><input type="text" placeholder='Predchozí klub' name="formerclub" value="<?php echo strip_tags($formerclub); ?>"
                                id="frmFClub" />
                </td>

        </tr>


        <tr>

                <td><input type="text" placeholder='Dátum narodenia DD.MM.RRRR' required="required" name="birth" value="<?php echo strip_tags($birth); ?>"
                                id="frmtBirth" />
                </td>
                <td>&nbsp;</td>


                <td>
                        <input type="text" placeholder='Trénerské vzdelanie' required="required" name="maxschool" value="<?php echo strip_tags($maxschool); ?>"
                                id="frmtBirth" />
                </td>
        </tr>
</table>
<p>&nbsp;</p>
<div style="font-size: 12px;line-height: 12px ; margin-top: 10px;  margin-left: 0; padding-left:  0px; padding-bottom: 0">
        
                <input type="checkbox" required="" class="required short " title="• Musíte zaškrtnout pole Souhlasím..." name="iamoknew" value="1" id="Musíte zaškrtnout pole Souhlasím..."> <strong>Súhlas so spracovaním osobných údajov</strong>  Týmto udeľujem   Spoločnosť Moderní fotbal s.r.o., so sídlom Drtinova 10/557, 150 00 Praha 5, IČO: 24851701, zapísaná v obchodnom registri vedenom Mestským súdom v Prahe pod spisovou značkou 201581 C, ako správca osobných údajov (ďalej len „spoločnosť Moderní fotbal”), <strong>slobodný a dobrovoľný súhlas so spracovaním osobných údajov v rozsahu:</strong>&nbsp;fakturačné údaje, meno a priezvisko, dátum narodenia, príslušnosť ku športovému oddielu, dosiahnuté trénerské vzdelanie, e-mail, telefón, obrazové aj zvukové záznamy mojej osoby a podobizne  <strong>za účelom: </strong>&nbsp;realizácie zakúpených služieb a produktov a oslovovanie s ponukou ďalších produktov a služieb spoločnosti Moderní fotbal, v rámci informovaní o produktoch, službách, akciách, súťažiach, odoberaniu noviniek a zasielaniu priania k meninám a narodeninám, ku robeniu fotografií a video dokumentácií z realizácie služieb a vedeniu účtovníctva.</p>
        <p>&nbsp;</p>
        <p ><strong>Súhlas je udelený na dobu 5 rokov.</strong><br /> <br /> Ďalej beriem na vedomie, že tento súhlas môžem kedykoľvek odvolať elektronicky na adrese  <a href="mailto:info@coerver.sk">info@coerver.sk</a> alebo písomne na adrese spoločnosti Moderní fotbal s.r.o. Súhlas môžem odvolať i pred uplynutím doby, na ktorú bol udelený. Odvolaním súhlasu nie je dotknutá zákonnosť spracovania osobných údajov pred odvolaním súhlasu. Všeobecné informácie  o prístupe k ochrane osobných údajov sú k dispozícii na webovej adrese  <a href="/osobne-udaje.html" target="_blank">https://www.coerver.sk/osobne-udaje</a>.</p>
                
        
          </div>