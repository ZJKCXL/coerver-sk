<?
    define("VISTOS_PASS","V3IcFLIm31A2Ho3");

    $asc2uni = Array();
    $xml = false;

    for($i=128;$i<256;$i++)
    {
        $asc2uni[chr($i)] = "&#x".dechex($i).";";
    }

    function XMLStrFormat($str)
    {
        global $asc2uni;
        $str = str_replace("&", "&amp;", $str);
        $str = str_replace("<", "&lt;", $str);
        $str = str_replace(">", "&gt;", $str);
        $str = str_replace('class="MsoNormal"', "", $str); 
        $str = str_replace("'", "&apos;", $str);
        $str = str_replace("\"", "&quot;", $str);
        $str = str_replace("\r", "", $str);
        //$str = strtr($str,$asc2uni);
        return $str;
    }

    function date_cz2us($in_date)
    {
        $exp = explode(".",$in_date);

        if (strlen(trim($exp[0])) == 0)
        {
            return false;
        }

        $stmp = mktime(0,0,1,$exp[1],$exp[0],$exp[2]);
        return date("Y-m-d",$stmp);
    }

    if (intval($_REQUEST["termin_id"])>0)
    {
        $termin_id = $_REQUEST["termin_id"];
    }

    if (isset($termin_id) && intval($termin_id)>0)
    {
        $link = @mysql_connect("81.0.235.144","coerver_remote","9879fgdfgpoiwer554");
        if (!$link)
        {
            exit;
        }
        @mysql_query("SET character_set_results=utf8");
        @mysql_query("SET character_set_connection=utf8"); 
        @mysql_query("SET character_set_client=utf8");
        $db = @mysql_select_db("coerver_remote");
        if (!$db)
        {
            exit;
        }

        $query = "SELECT * FROM cz_reg_youth WHERE ID = ".$termin_id;

        $res = @mysql_query($query);
        if ($res && @mysql_num_rows($res)>0)
        {
            $row = @mysql_fetch_array($res);
            $xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
            $xml .= "<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">";
            $xml .= "<soap12:Body>";
            $xml .= "<InsertKurzRegistrace xmlns=\"http://crm.vistoscrm.cz/Coerver/\">";
            $xml .= "<Jmeno>".XMLStrFormat($row["name"])."</Jmeno>";
            $xml .= "<Prijmeni>".XMLStrFormat($row["surname"])."</Prijmeni>";
            $xml .= "<DatumNarozeni>".XMLStrFormat(date_cz2us($row["birth"]))."</DatumNarozeni>";
            $xml .= "<Klub>".XMLStrFormat($row["club"])."</Klub>";
            $xml .= "<PredchoziKlub>".XMLStrFormat($row["club2"])."</PredchoziKlub>";
            $xml .= "<NejvyssiVzdelani>".XMLStrFormat($row["edu"])."</NejvyssiVzdelani>";
            $xml .= "<Adresa>";
            $xml .= "<street>".XMLStrFormat($row["street"])."</street>";
            $xml .= "<town>".XMLStrFormat($row["town"])."</town>";
            $xml .= "<zip>".XMLStrFormat($row["zip"])."</zip>";
            $xml .= "<phone>".XMLStrFormat($row["phone"])."</phone>";
            $xml .= "<email>".XMLStrFormat($row["email"])."</email>";
            $xml .= "</Adresa>";

            $terminquery = "SELECT * ";
            $terminquery .= " FROM cz_terms_youth, cz_reg_terms_youth ";
            $terminquery .= " WHERE cz_terms_youth.ID = cz_reg_terms_youth.youthTerm";
            $terminquery .= " AND cz_reg_terms_youth.youthUser = ".$row["ID"];
            $terminres = @mysql_query($terminquery);
            if ($terminres && @mysql_num_rows($terminres)>0)
            {
                $xml .= "<Termin>";
                while ($terminresarr = @mysql_fetch_array($terminres))
                { 
                    $xml .= "<from>".XMLStrFormat($terminresarr["youthFrom"])."</from>";
                    $xml .= "<to>".XMLStrFormat($terminresarr["youthTo"])."</to>";
                    $xml .= "<place>".XMLStrFormat($terminresarr["youthPlace"])."</place>";
                    $xml .= "<ProductCode>".XMLStrFormat($terminresarr["youthVistosCode"])."</ProductCode>";
                }
                $xml .= "</Termin>";
            }

            $xml .= "<Country>CZ</Country>";
            $xml .= "<Password>".VISTOS_PASS."</Password>";
            $xml .= "</InsertKurzRegistrace>";
            $xml .= "</soap12:Body>";
            $xml .= "</soap12:Envelope>";
        }
    }
    
    if (strlen($xml)>0)
    {
        $log_file = @fopen("../logs/xml_request.log","a");
        @fwrite($log_file,date("Y-m-d H:i:s")." ".$xml_document."\n");
        @fflush($log_file);
        @fclose($log_file);

        $url_conn = @curl_init();

        $url = "http://crm.vistoscrm.cz/Coerver_Interface/CoerverService.asmx";

        curl_setopt($url_conn,CURLOPT_URL, $url);
        curl_setopt($url_conn,CURLOPT_POST,1);
        curl_setopt($url_conn,CURLOPT_POSTFIELDS,$xml);
        curl_setopt($url_conn,CURLOPT_HEADER,1);
        curl_setopt($url_conn, CURLOPT_HTTPHEADER, array("Content-Type: application/soap+xml; charset=utf-8","Content-Length: ".strlen($xml)));
        curl_setopt($url_conn,CURLOPT_RETURNTRANSFER,1);

        $data = @curl_exec($url_conn);

        //$log_file = fopen("../logs/xml_response.log","a");
                
        preg_match_all('/HTTP\/.* ([0-9]+) .*/', $data, $status);
        
        if ($status[1][0] == "200" or $status[1][1] == "200")
        {
                        
            $response_lines = explode("\n",$data);
            $xml_response = false;
            
            foreach ($response_lines as $response)
            {
                if (substr($response,0,5) == "<?xml")
                {
                    $xml_response = $response;
                    break;
                }
            }

            if ($xml_response)
            {
                /* $xml_parser = @xml_parser_create();
                // use case-folding so we are sure to find the tag in $map_array
                @xml_set_character_data_handler($xml_parser, "characterData");
                @xml_set_element_handler($xml_parser, "startElement", "endElement");

                @xml_parse($xml_parser,$xml_response,true);

                @xml_parser_free($xml_parser);
                */ //@fwrite($log_file,date("Y-m-d H:i:s")." ".$xml_response."\n");

                echo $xml_response;
                
                //Tady je ta XML odpoved od nich, ja nevim co s ni budeme delat. Mozna ze nic, mozna ukladat.

            }
            else
            {
                //@fwrite($log_file,date("Y-m-d H:i:s")." ".print_r($data,true)."\n");
            }

        }
        else
        {
            //@fwrite($log_file,date("Y-m-d H:i:s")." ".print_r($data,true)."\n");            
        }

        //@fflush($log_file);
        //@fclose($log_file);


    }

?>
