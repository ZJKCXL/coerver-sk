<?php
header("content-type: text/html; charset=utf-8");
$page = @$_REQUEST['page'];
$section = @$_REQUEST['section'];
$note = @$_REQUEST['note'];
$blabla = @$_REQUEST['blabla'];
$lang = 'en';
include_once("config/conf.protected.php");
include_once("config/conf.php");
include "./sql/endb.php";
include "./lib/pageswitcher.php";
include "./lib/lib.php";
include "./lib/en_seo.php";
include "./lib/pager.php";
include("./lib/user_functions.php");
include("./lib/StringUtils.php");
$time = time();
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <title><?php echo pageWTF(@$finalpage,@$page,@$finalsection,@$finalID,@$finalnewsid,'title'); ?></title>
    <meta name='description' content='<?php echo pageWTF(@$finalpage,@$page,@$finalsection,@$finalID,@$finalnewsid,'desc'); ?>' />
    <meta name='keywords' content='<?php echo pageWTF(@$finalpage,@$page,@$finalsection,@$finalID,@$finalnewsid,'keywords'); ?>' />
    <!-- Facebook -->
    <?php
    if(strlen(pageWTF(@$finalpage,@$page,@$finalsection,@$finalID,@$finalnewsid,'fbimg'))>0){
    ?><meta property="og:image" content="<?php echo pageWTF(@$finalpage,@$page,@$finalsection,@$finalID,@$finalnewsid,'fbimg'); ?>" />
    <?php
    }
    ?><meta property="og:title" content="<?php echo pageWTF(@$finalpage,@$page,@$finalsection,@$finalID,@$finalnewsid, 'title'); ?>"/>
    <meta property="og:url" content="<?php  echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>"/>
    <meta property='og:description' content='<?php echo pageWTF(@$finalpage,@$page,@$finalsection,@$finalID,@$finalnewsid, 'desc'); ?>' />
    <meta property="og:type" content="website" />
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo $tempdir; ?>/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="<?php echo $tempdir; ?>/favicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="<?php echo $tempdir; ?>/favicons/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="<?php echo $tempdir; ?>/favicons/manifest.json">
    <link rel="mask-icon" href="<?php echo $tempdir; ?>/favicons/safari-pinned-tab.svg" color="#be0819">
    <link rel="shortcut icon" href="<?php echo $tempdir; ?>/favicons/favicon.ico">
    <meta name="msapplication-config" content="<?php echo $tempdir; ?>/favicons/browserconfig.xml">
    <meta name="theme-color" content="#be0819">
    <link rel="stylesheet" href="<?php echo $tempdir; ?>/font-awesome/css/font-awesome.min.css">    
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i&amp;subset=latin-ext" rel="stylesheet" />
    <link href="<?php echo $tempdir; ?>/css/parallax.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $tempdir; ?>/css/layout.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $tempdir; ?>/css/response.css" rel="stylesheet" type="text/css" />
    <?php echo pageWTF ($finalpage,$page,$finalsection,$finalID,$finalnewsid,'css'); ?>
    <meta name="robots" content="index,follow" />
    <script src="<?php echo $tempdir; ?>/js/jquery/jquery.js" type="text/javascript"></script>
    <script src="<?php echo $tempdir; ?>/js/ui/jquery-ui.js" type="text/javascript"></script>
    <script src="<?php echo $tempdir; ?>/js/scripts.js"   defer></script>
   <?php
    if( $ishome == 1){
    }else{
    ?>
        <link href="<?php echo $tempdir; ?>/css/responsePage.css" rel="stylesheet" type="text/css" />
    <?php
    }
    ?>
</head>
<body>
    <nav><div>
            <?php
            if( $ishome == 1){
            ?>
                <h1 id='logo'><a href='<?php echo $langtemp; ?>/'><span><?php echo $webname; ?></span></a></h1>
                <?php
            }else{
                ?>
                <div id='logo'><a href='<?php echo $langtemp; ?>/'><span><?php echo $webname; ?></span></a></div>
            <?php
            }
            ?>
            <ul id="menu">
                <?php
                $type = 0;
                include ('./eninside/menu.php');
                ?>
            </ul>
            <span id='startmenu' title='MENU'></span>
            <ul class="mini-menu" id="mini-menu">
                <?php
                $type = 0;
                include ('./eninside/menu.php');
                ?>
            </ul>
        </div>
    </nav>
    <div id='wrapper'>
    <?php
    include("./eninside/slider.php");
    ?>
    <div id='main'>
    <?php
    include ('./eninside/content.php');
    ?>
    </div>
    <footer>
        <div>
         <span>
            <h4>Contact:   </h4>
                    <?php
                    echo $finalpage;
                    include ('./adminpages/microdata_footer.php');
                    ?>
                </span>
                <span>
                </span>

                <span class='right'>
                &copy; <?php echo date("Y"); ?> Formic Construction s.r.o.<br/>by <a href='http://www.honeypot.cz'>honeypot.cz</a> 
                </span>
                <div class='mainclr'> </div>
            </div>
        </footer>
    </div>
    		 <div id="langs">
				<span class="cz"><a href="/2017/" title="česky"><em >&nbsp;</em>CZ</a></span>
				<span class="en"><em  class="active">&nbsp;</em>EN</span>
			</div>
</body>