html, body {
font-family:  sans-serif;
 color: #fff;
 font-size: 90%;
 background-color: #000;
}
a{
	color:  #75B338;
}
a:hover{
 color:  #75B338!important;
 text-decoration: none;
}
p {margin:0; padding:0; text-align: left}
pre {

	font-size: 13px;
}
h2{
 color: #75B338;           
 font-size: 190%;
 _font-size: 160%;
  font-weight: bold;
  font-style: italic;
   text-transform: uppercase;  
 margin-top: 0;                          
 margin-bottom: 0.5em;
font-family:  sans-serif;
}
#sidebox h2{
 font-size: 120%;
}
h3{
  font-size: 130%;
  font-weight: normal;
  color:  #75B338;
}
.minibox h3{
   font-family:  sans-serif;
}
h4{                    
color: #B7132E;
font-size: 105%;
margin-bottom: 0;
margin-top: 0;
}
h5{
 margin: 0;
}

#text td{
vertical-align:top;
}
ul{
 list-style-type: square;
}
 li{
 color: #75B338;
}
#content li span, #content li label{
 color: #fff ;
}
form{
 margin: 0;
 padding: 0;
}
fieldset{
 border: none;
}
#content #text  a.gal img{
 float: left;
 padding: 2px;
 border: 1px solid #e7e7e7;
 margin: 5px 5px 0 0;     
 padding: 5px;
 width: 135px;
}
.imggal{
 margin-top: 1em;
}
#content #text #intext .pparts{
 clear: both;
 margin-top: 0.5em;

}
.mainclr{
 clear: both;
 margin: 0;
 padding: 0;
 height: 0;
 font-size: 0;
 padding: 0;
 height: 0;
 
}
table, tr, th, td {
	border-collapse: collapse;
	border: 1px solid #CFD4DA;
}
table {
	border: 5px solid #CFD4DA;
}
th, td {
	padding: 5px 8px;
	text-align: left;
	vertical-align: top;
}
#content  ul, #content li{
 margin-left: 0;
 padding: 0;
}
#content ul{
 margin-left: 1.5em;
}
#content #sidebox a.new_window_link img, #content a.new_window_link img, img.downimg, #map_canvas img {
 border: none!important;
 padding: 0!important;
 background: transparent!important;
}

#map_canvas{
 border: 1px solid #fff;
 overflow: hidden;
 margin: 1em 0;
}
.sbmenu{
 margin-top: 0px;
}
.banner{
 border: 1px solid red;
}
p.odpaly{
 margin: 10px 15px;
}
#content #text .ref fieldset p{
 margin: 10px;
}

.required {
	background: #EDFFF0 url('../images/required.gif') top right no-repeat;
}
.invalid {
	background:  #D1D8DE  url('../images/wrong.gif') top right no-repeat;
	color: red;
}
select.required {
	background: #fff url('../images/required.gif') top left no-repeat;
}
select.invalid {
	background: #fff url('../images/wrong.gif') top left no-repeat;
	color: red;
}
.hibox{

  
 
}
ul ul{
 list-style-type: circle
}
.hibox{
 display: block; 
background: #FED11C;
margin: 35px 20px 10px 20px;;
text-align: center;
color: #d83e02;
}
.hibox a{
 color: #d83e02;
 font-size: 150%;
}
ol li{
 margin-bottom: 0.5em;
}

.hibox_leader a.log, .hibox_work a.log,  .hibox_market a.log, .hibox_komunita a.log, .hibox_green a.log, , .hibox_vize a.log, .hibox_o2 a.log, .hibox_leader a.more, .hibox_work a.more, .hibox_komunita a.more, .hibox_market a.more, .hibox_green a.more, .hibox_vize a.more, .hibox_o2 a.more{
 position: absolute;
 display: block;
 bottom: -8px;
 right: 80px;
 line-height: 16px;
 width: 60px;
 font-size: 11px;
 text-align: center;
 background: #323E7A;
 color: #fff;
 text-decoration: none;
 
}
.hibox_leader a.more, .hibox_work a.more, .hibox_komunita a.more, .hibox_market a.more, .hibox_green a.more, .hibox_vize a.more, .hibox_o2 a.more{
 right: 10px;
}
.hibox_leader a.log:hover, .hibox_work a.log:hover, .hibox_market a.log:hover, .hibox_komunita a.log:hover, .hibox_green a.log:hover, .hibox_vize a.log:hover, .hibox_o2 a.log:hover, .hibox_leader a.more:hover, .hibox_work a.more:hover, .hibox_market a.more:hover, .hibox_komunita a.more:hover, .hibox_green a.more:hover, .hibox_vize a.more:hover, .hibox_o2 a.more:hover{
 color: #323E7A!important;
 text-decoration: underline;
 background:  #e8ecfc;

}

.hibox_leader{
border: 2px solid #323E7A;
border-width: 0;
background: #e8ecfc url('../images/ico_leader.jpg') no-repeat right top;
padding: 1em 1em 2em 1em;
position: relative;
}
.hibox_leader h4{
 color: #323E7A;
}
.hibox_leader a, .hibox_leader a:hover{
 color: #323E7A;
}
.hibox_work{
border: 2px solid #7e123f;
border-width: 0;
background: #fef0f7 url('../images/ico_work.jpg') no-repeat right top;
padding: 1em 1em 1.5em 1em;
position: relative;
}
.hibox_work h4, .hibox_work a, .hibox_work a:hover{
 color:#7e123f!important;
}
.hibox_work a.log, .hibox_work a.more {
 background: #7e123f;
}
.hibox_work a.log:hover, .hibox_work a.more:hover{
 background: #fef0f7;
 color:#7e123f!important;;
}
.hibox_komunita{
border: 2px solid #da741b;
border-width: 0;
background: #fdf5ed url('../images/ico_komunita.jpg') no-repeat right top;
padding: 1em 1em 2em 1em;
position: relative;
}
.hibox_komunita h4, .hibox_komunita a, .hibox_komunita a:hover{
 color:#da741b!important;
}
.hibox_komunita a.log, .hibox_komunita a.more {
 background: #da741b!important;
}
.hibox_komunita a.log:hover, .hibox_komunita a.more:hover{
 background: #fdf5ed;
 color:#da741b!important;
}
.hibox_green{
border: 2px solid #6ea94f;
border-width: 0;
background: #ecfae4 url('../images/ico_green.jpg') no-repeat right top;
padding: 1em 1em 2em 1em;
position: relative;
}
.hibox_green h4, .hibox_green a,  .hibox_green a:hover{
 color:#6ea94f!important;
}
.hibox_green a.log, .hibox_green a.more {
 background: #6ea94f;
}
.hibox_green a.log:hover, .hibox_green a.more:hover{
 background: #ecfae4;
 color:#6ea94f!important;;
}

  .hibox_vize{
border: 2px solid #5F6678;
border-width: 0;
background: #ECF0F3 url('../images/vize.jpg') no-repeat right top;
padding: 1em 1em 2em 1em;
position: relative;
}
.hibox_vize h4, .hibox_vize a{
 color:#5F6678;
}
 
.hibox_vize a.log, .hibox_vize a.more, .hibox_vize a.more:hover {
 background: #ECF0F3;
}
.hibox_vize a.log:hover, .hibox_vize a.more:hover{
 background: #ecfae4;
 color:#6ea94f!important;;
}

.hibox_o2{
border: 2px solid #1f122e;
border-width: 0;
background: #c9e0f1 url('../images/ico_o2.jpg') no-repeat right top;
padding: 1em 1em 2em 1em;
 position: relative;
}
.hibox_o2 h4{
 color:#1f122e;
}
.hibox_o2 a.log, .hibox_o2 a.more {
 background: #1f122e;
}
.hibox_o2 a.log:hover, .hibox_o2 a.more:hover{
 background: #c9e0f1;
 color:#1f122e!important;
}


.hibox_csr{
border: 2px solid #1f122e;
border-width: 0;
background: #EFF9B2 url('../images/ico_csr.jpg') no-repeat right top;
padding: 1em 1em 2em 1em;
 position: relative;
 margin-bottom: 10px;
}
.hibox_csr h3{
  margin-top: 0;
}
.hibox_csr a.log, .hibox_csr a.more {
 background: #1f122e;
}
.hibox_csr a.log:hover, .hibox_csr a.more:hover{
 background: #c9e0f1;
 color:#1f122e!important;
}

.hibox_market{
border: 2px solid #259b9f;
border-width: 0;
background: #e5efef url('../images/ico_market.jpg') no-repeat right top;
padding: 1em 1em 1.5em 1em;
position: relative;
}
.hibox_market h4{
 color:#259b9f;
}
.hibox_market a.log, .hibox_market a.more {
 background:#259b9f;
}
.hibox_market a.log:hover, .hibox_market a.more:hover{
 background: #e5efef;
 color:#259b9f!important;;
}
.hibox_market h4, .hibox_market a, .hibox_market a:hover {
 color:#259b9f;
}

.hibox_bench{
border: 2px solid #1d5d76;
border-width: 0;
background: #f1fafd url('../images/ico_bench.jpg') no-repeat right top;
padding: 1em 1em 1.5em 1em;
position: relative;
}
.hibox_bench h4{
 color: #1d5d76;
}
.hibox_bench a.log, .hibox_bench a.more {
 background: #1d5d76;
}
.hibox_bench a.log:hover, .hibox_bench a.more:hover{
 background: #f1fafd;
 color:#5F6678!important;
}
.hibox_leader a.log, .hibox_work a.log,  .hibox_market a.log, .hibox_komunita a.log, .hibox_green a.log, .hibox_vize a.log, .hibox_o2 a.log {
display: none;
}
/* novinky matejska */

.program1 h3, .program1 h3 a, .program1 h3 a:hover, .xprogram1 h3, .xprogram1 h3 a, .xprogram1 h3 a:hover{
  color: #7e123f!important;
}
.program1{
background: #fef0f7 url('../images/ico_work.jpg') no-repeat right top;
}

.program2 h3, .program2 h3 a, .program2 h3 a:hover,  .xprogram2 h3, .xprogram2 h3 a, .xprogram2 h3 a:hover{
  color: #259b9f!important;
}
.program2,{
background: #e5efef url('../images/ico_market.jpg') no-repeat right top;
}

.program3 h3, .program3 h3 a, .program3 h3 a:hover,  .xprogram3 h3, .xprogram3 h3 a, .xprogram3 h3 a:hover{
  color: #6ea94f!important;
}
.program3{
 background: #ecfae4 url('../images/ico_green.jpg') no-repeat right top;
}


.program4 h3, .program4 h3 a, .program4 h3 a:hover, .xprogram4 h3, .xprogram4 h3 a, .xprogram4 h3 a:hover{
 color:#da741b;
}
.program4{
background: #fdf5ed url('../images/ico_komunita.jpg') no-repeat right top;
}

.program5 h3, .program5 h3 a, .program5 h3 a:hover, .xprogram5 h3, .xprogram5 h3 a, .xprogram5 h3 a:hover{
  color: #1d5d76!important;
}
.program5{
background: #f1fafd url('../images/ico_bench.jpg') no-repeat right top;
}
.hibox_green, .hibox_vize, .hibox_market, .hibox_work, .hibox_komunita , .hibox_leader {
 margin-bottom: 10px;
}
.hibox_green h3, .hibox_vize h3, .hibox_market h3, .hibox_work h3, .hibox_komunita h3, .hibox_leader h3 {
 margin-top: 0px;
}
.odskok{
 margin-top: 2em;
}
#sidebox .kat{
 padding: 5px;
 font-size: 90%;

}
#sidebox .kat span{
 font-size: 85%;
 margin-bottom: 5px;
 display: block;
}
#sidebox .kat span b{
 font-size: 110%;

}
.xseachinput em{
 display: block;
 floaT: left;
 width: 1.5em;
 text-align: center;
}
.xseachinput em.longer{
 display: block;
 floaT: left;
 width: 5em;
 text-align: left;
 margin: 0 0.5em 1em ;
 padding-right: 1em;
 border-right: 1px solid #efefef;

}
.cuk{
 /*margin-left: 1em;*/
}
.mytab{
 width: 700px;
 overflow: auto;
 background: #fff;
}
.hibox_komunita a.alltext, .hibox_work a.alltext, .hibox_green a.alltext, .hibox_vize a.alltext, .hibox_market a.alltext {
 text-decoration: underline;
 float: right;
 cursor: pointer;
}
.hibox_komunita a.alltext:hover, .hibox_work a.alltext:hover, .hibox_green a.alltext:hover, .hibox_vize a.alltext:hover, .hibox_market a.alltext:hover {
 text-decoration: none;
 float: right;
}
.new{
  background: #ffcbd5 ;
  border: 1px solid red;
  color: red;
  font-size: 95%;
  padding:  1px 3px;
  margin: 0 5px;
}
.euro {
  background: #a1a3ff ;
  border: 1px solid blue;
  color:  blue;
  font-size: 90%;
  padding:  1px 3px;

}
hr{
 
  border: none;
  background: none;
  border-top: 1px dotted #4B646C 
}
ul.odpaltrochu li{
 margin-bottom: 0.5em
}
textarea {
font-family:  sans-serif;
 color: #4b646c;
 font-size: 85%;
 }
 .hibox_csr li {
  padding-bottom: 5px;
  margin-bottom: 5px;
  border-bottom: 2px dotted #D5ED9F  ;
  margin-right:10px;
 }